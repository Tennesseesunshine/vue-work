import vm from '../../main'

// 确认提示的一些公共配置
const confirm = (type = 'warning') => {
  return {
    confirmButtonText: vm.$t('ViBorder.common.sure'),
    closeOnClickModal: false,
    closeOnPressEscape: false,
    showCancelButton: true,
    cancelButtonText: vm.$t('ViBorder.common.cancel'),
    type
  }
}
// 其他系统的公共数据
const otherSystem = () => {
  return [
    {
      title: vm.$t('ViBorder.DIC.INSPECTSTEP.ANIMALCHECK'),
      conclusionTime: '',
      type: 'animalCheck',
      conclusion: {},
      tipInfo: []
    },
    {
      title: vm.$t('ViBorder.DIC.INSPECTSTEP.PLANTCHECK'),
      conclusionTime: '',
      type: 'plantCheck',
      conclusion: {},
      tipInfo: []
    },
    {
      title: vm.$t('ViBorder.DIC.INSPECTSTEP.HEALTHCHECK'),
      conclusionTime: '',
      type: 'healthCheck',
      conclusion: {},
      tipInfo: []
    },
    {
      title: vm.$t('ViBorder.DIC.INSPECTSTEP.TRAFFICCONFIRM'),
      conclusionTime: '',
      type: 'trafficConfirm',
      conclusion: {},
      tipInfo: []
    }
  ]
}
export default {
  confirm,
  otherSystem
}
