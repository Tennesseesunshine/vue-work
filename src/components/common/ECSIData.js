/**
 * 策略模式导出数据 0代表综合查验 1代表人工查验 2代表综合查验和人工查验
 */
import _get from 'lodash/get'
import vm from '../../main'

const uri = `${window.config.baseUrl}${window.config.fileServerUrl}${window.config.fileFolder.niiFolder}`

const initPath = (result) => {
  let getData = _get(result, 'measureVO.vehicleXray.savePath', '')
  let xRayPath = getData ? getData.split(',') : ''
  let responsePathStr = xRayPath && xRayPath[0]
  let responsePath = responsePathStr && responsePathStr.substring(0, responsePathStr.lastIndexOf('/') + 1)
  let path = ''
  let scanId = _get(result, 'measureVO.vehicleXray.scanId', '')
  let ICON = `${responsePath}${scanId}_icon.jpg`
  let JPG = `${responsePath}${scanId}.jpg`
  if (result.suspicionMark) {
    path = 'grab_img_suspects.jpg'
  } else {
    if (xRayPath) {
      if (xRayPath.includes(ICON) && !path) {
        path = `${scanId}_icon.jpg`
      } else if (xRayPath.includes(JPG) && !path) {
        path = `${scanId}.jpg`
      } else {
        path = ''
      }
    }
  }
  return path ? `${uri}${responsePath}${path}` : ''
}
/**
 *
 * @param p代表属性 p0 ,p1, p2 分别为综合，人工，两者
 * 接受参数result type为012类型
 */
const ECSI = ({result, type, mainTitle}) => {
  let obj = {
    mainTitle: mainTitle,
    checkUrl: initPath(result),
    licensePlateNumber: _get(result, 'checkInVO.vehicleCheckIn.licensePlateNumber', '')
  }
  let initData = {
    // 综合查验
    p0: () => {
      return {
        dcContentList: [
          {
            title: vm.$t('ViBorder.businessLogicCommon.lpnFront'),
            value: vm.$_get(result, 'checkInVO.vehicleCheckIn.licensePlateNumber', '')
          },
          {
            title: vm.$t('ViBorder.businessLogicCommon.dcConclusion'),
            value: vm.$t(vm.$_get(result, 'dcConclusion.conclusionType.key', ''))
          },
          {
            title: vm.$t('ViBorder.DC.inspectionDcTime'),
            value: vm.$formatDateTime(vm.$_get(result, 'dcConclusion.conclusionTime', ''), vm.$t('ViBorder.common.formatDate'))
          },
          {
            title: vm.$t('ViBorder.DC.inspectionDcUser'),
            value: vm.$_get(result, 'dcConclusion.operationUserName', '')
          },
          {
            title: vm.$t('ViBorder.businessLogicCommon.inspectionDcRemark'),
            value: vm.$_get(result, 'dcConclusion.conclusionRemark', '')
          }
        ]
      }
    },
    p1: () => {
      return {
        piContentList: [
          {
            title: vm.$t('ViBorder.businessLogicCommon.inspectionPiConclusion'),
            value: vm.$t(vm.$_get(result, 'piConclusion.conclusionType.key', ''))
          },
          {
            title: vm.$t('ViBorder.PI.inspectionPiTime'),
            value: vm.$formatDateTime(vm.$_get(result, 'piConclusion.conclusionTime', ''), vm.$t('ViBorder.common.formatDate'))
          },
          {
            title: vm.$t('ViBorder.PI.inspectionPiUser'),
            value: vm.$_get(result, 'piConclusion.operationUserName', '')
          },
          {
            title: vm.$t('ViBorder.businessLogicCommon.inspectionPiRemark'),
            value: vm.$_get(result, 'piConclusion.conclusionRemark', '')
          }
        ]
      }
    },
    p2: function () {
      return {
        ...this.p0(), ...this.p1()
      }
    }
  }
  let contentList = initData[`p${type}`]()
  // console.log({ ...obj, ...contentList })
  return { ...obj, ...contentList }
}
export default ECSI
