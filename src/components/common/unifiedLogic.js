import Vue from 'vue'
import get from 'lodash/get'
import vm from '../../main'
import { formatDateTime } from '../../lib/index'
import initOtherSysData from './commonData'
// 获取审批记录
const getConclusion = (parm = []) => {
  let conclusion = []
  parm.length > 0 &&
    parm.forEach((item, index) => {
      conclusion.push(
        `${vm.$t('ViBorder.businessLogicCommon.conclusion')}: ${vm.$t(get(item, 'conclusionType.key', ''))}<br/>
        ${vm.$t('ViBorder.businessLogicCommon.reason')}: ${get(item, 'conclusionRemark', '') || ''}<br/>
        ${vm.$t('ViBorder.IC.operator')}: ${item.operationUserName}<br/>
        ${vm.$t('ViBorder.IC.operationTime')}: ${formatDateTime(item.operationTime, vm.$t('ViBorder.common.formatDate'))}<br/>
        ${index === parm.length - 1 ? '' : `<br/>`}`
      )
    })
  return conclusion.join(',').replace(/,/g, '')
}
const otherHoverData = (data = {}, array = initOtherSysData.otherSystem()) => {
  array.forEach(e => (e.tipInfo = []))
  for (const oKey in data) {
    if (data.hasOwnProperty(oKey)) {
      const element = data[oKey]
      array.forEach(item => {
        if (oKey === item.type) {
          element.forEach((ele, idx) => {
            if (idx === 0) {
              item.conclusionTime = formatDateTime(
                ele.operationTime,
                vm.$t('ViBorder.common.formatDate')
              )
              item.conclusion = {
                key: vm.$t(get(ele, 'conclusionType.key', '')),
                value: get(ele, 'conclusionType.value', '')
              }
            }
            item.tipInfo.push(ele)
          })
        }
      })
    }
  }
  array.forEach(item => {
    item.tipInfo = array.length > 0 && getConclusion(item.tipInfo || [])
  })
  return array
}
export { getConclusion, otherHoverData }
