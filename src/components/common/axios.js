/* eslint-disable */
/**
 * 封装axios请求方法
 */
const axios = require('axios')
const qs = require('qs')
import { Notification } from 'element-ui'
const elementcss = require('element-ui/lib/theme-chalk/index.css')
const config = window.config // config配置文件
import vm from '@/main.js'
var cancel
var errorCodeArr = []
var promiseArr = {}
let notifyOffset = window.config.notifyOffset
// 添加请求拦截器
axios.interceptors.request.use(
  function(config) {
    // 系统设置保存之前设置将数据变为[]
    let uri = config.url
      .split('/')
      .splice(1)
      .join('/')
    let viborderProjectName = window.config.projectName
    let vitrackingProjectName = window.config.Vitracking.commonUrl
    if (config.url.includes(viborderProjectName)) {
      // 审计日志去掉api
      if (config.url.includes(`/log/`)) {
        config.url = `${window.config.projectName}/${uri}`
      } else {
        config.url = `${window.config.projectName}/api/${uri}`
      }
    } else {
      if (!config.url.includes(vitrackingProjectName)) {
        config.url = `/${window.config.upms}/${uri}`
      }
    }
    // console.log(' config.url', config.url)
    if (config.url.includes('/exportFile/')) {
      config.timeout = window.config.lotOfDataTimeout
    }
    if (config.data) {
      for (let i in config.data) {
        if (i === 'updateSetting') {
          let arr = []
          Object.values(config.data).forEach(e => {
            arr.push(e)
          })
          arr.pop()
          config.data = arr
        }
      }
    }
    // 在发送请求之前做些什么
    // 能做的事如下 检查权限 增加页面loading  网络状态判断等
    if (promiseArr[config.url]) {
      promiseArr[config.url]('cancel')
      promiseArr[config.url] = cancel
    } else {
      promiseArr[config.url] = cancel
    }
    return config
  },
  function(error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
axios.interceptors.response.use(
  function(response) {
    // 对响应数据做点什么
    delete promiseArr[response.config.url.replace(response.config.baseURL, '')]
    return response
  },
  function(error) {
    // 对响应错误做点什么
    // 例如用户请求失效，返回登录页什么的
    return Promise.reject(error)
  }
)

function error(response) {
  // 500这里
  console.log('error catch', response)
  if (response && response.status !== 200) {
    console.log(response.message)
    if (response.message === 'cancel') {
    } else {
      Notification.error({
        title: vm.$t('ViBorder.common.dataError'),
        message: vm.$t('ViBorder.common.NetworkError'),
        offset: notifyOffset
      })
    }
    return response
    // 如果不需要除了data之外的数据，可以直接 return response.data
  } else {
    Notification.error({
      title: vm.$t('ViBorder.common.dataError'),
      message: vm.$t('ViBorder.common.NetworkError'),
      offset: notifyOffset
    })
  }
}
let closeTips = res => {
  for (var i = 0; i < errorCodeArr.length; i++) {
    if (errorCodeArr[i] === res.data.errorCode) {
      errorCodeArr.splice(i, 1)
    }
  }
}
function success(res) {
  // console.log('response这是success', res.data.errorCode)
  let { flag, errorCode } = res.data
  if (!flag && errorCode) {
    // upms警告
    if (errorCode.substring(3, 0) == 'W01') {
      errorCodeArr.push(errorCode)
      Notification.warning({
        title: vm.$t('ViBorder.common.warning'),
        message: vm.$t('ViBorder.upms.ajax.' + errorCode),
        onClose: closeTips(res),
        offset: notifyOffset
      })
      return false
    } else if (errorCode.substring(3, 0) == 'I01') {
      if (errorCode === 'I010100') {
        vm.$router.push({ path: '/login' })
      } else if (errorCode === 'I010117') {
        Notification.warning({
          title: vm.$t('ViBorder.common.warning'),
          message: vm.$t('ViBorder.upms.ajax.' + errorCode, {msg: res.data.result.leftTimes}),
          offset: notifyOffset
        })
        return false
      }
      // upms提示
      errorCodeArr.push(errorCode)
      Notification.info({
        title: vm.$t('ViBorder.common.prompt'),
        message: vm.$t('ViBorder.upms.ajax.' + errorCode),
        onClose: closeTips(res),
        offset: notifyOffset
      })
      return false
    } else if (errorCode.substring(3, 0) == 'E02') {
      // upms错误登录方面
      if (
        errorCode === 'E020001' &&
        res.config.url.indexOf('/user/isLogon') === -1
      ) {
        errorCodeArr.push(errorCode)
        localStorage.removeItem('Xcsrftoken')
        sessionStorage.removeItem('account')
        sessionStorage.removeItem('userName')
        Notification.error({
          title: vm.$t('ViBorder.common.dataError'),
          message: vm.$t('ViBorder.upms.ajax.' + errorCode),
          onClose: closeTips(res),
          offset: notifyOffset
        })
        vm.$router.push({ path: '/login' })
        return false
      } else {
        // upms内部错误
        errorCodeArr.push(errorCode)
        Notification.error({
          title: vm.$t('ViBorder.common.dataError'),
          message: vm.$t('ViBorder.upms.ajax.' + errorCode),
          onClose: closeTips(res),
          offset: notifyOffset
        })
        return false
      }
    } else if (errorCode.substring(3, 0) == 'E00') {
      // upms内部错误
      errorCodeArr.push(errorCode)
      Notification.error({
        title: vm.$t('ViBorder.common.dataError'),
        message: vm.$t('ViBorder.upms.ajax.' + errorCode),
        onClose: closeTips(res),
        offset: notifyOffset
      })
      return false
    } else if (errorCode.substring(3, 0) == 'E25') {
      // 后台接口错误
      errorCodeArr.push(errorCode)
      Notification.error({
        title: vm.$t('ViBorder.common.dataError'),
        message: vm.$t('ViBorder.errorCode.' + errorCode),
        onClose: closeTips(res),
        offset: notifyOffset
      })
    } else if (errorCode.substring(3, 0) == 'W25') {
      // 后台接口警告
      errorCodeArr.push(errorCode)
      Notification.warning({
        title: vm.$t('ViBorder.common.warning'),
        message: vm.$t('ViBorder.errorCode.' + errorCode),
        onClose: closeTips(res),
        offset: notifyOffset
      })
    } else if (errorCode.substring(3, 0) == 'I25') {
      // 后台接口提示
      errorCodeArr.push(errorCode)
      Notification.info({
        title: vm.$t('ViBorder.common.prompt'),
        message: vm.$t('ViBorder.errorCode.' + errorCode),
        onClose: closeTips(res),
        offset: notifyOffset
      })
    } else if (!errorCode) {
      Notification.error({
        title: vm.$t('ViBorder.common.prompt'),
        message: vm.$t('ViBorder.errorCode.E250001'),
        onClose: closeTips(res),
        offset: notifyOffset
      })
    }
  }
  // if (errorCodeArr.indexOf(res.data.errorCode) === -1) {
  // }
}
const $axios = (opts, data, responseType = 'json') => {
  // 去除字符串参数的左右空格
  if (data != null) {
    let keys = Object.keys(data)
    keys.forEach(e => {
      if (typeof data[e] === 'string') {
        data[e] = data[e].replace(/(^\s*)|(\s*$)/g, '')
      }
    })
  }
  let Public = {} //用于存放公共参数，类似于当前用户id等
  let defaultHeaders = {
    'X-REQUEST-LANGUAGE': localStorage.getItem('lang'),
    'X-ALL-LANGUAGES': 'ru-RU,en-US',
    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
  }
  let regExpMethod = /put|delete|post/i
  let httpDefaultOpts = {
    //http默认配置
    baseURL: config.baseUrl,
    timeout: config.timeout,
    method: opts.method,
    url: opts.url,
    responseType, // 一般一个网站的responseType 都是一样的
    withCredentials: true, // 是否允许带cookie这些
    arrayFormat: opts.arrayFormat, //有三个参数 'indices' id[0]=b&id[1]=c  'brackets' 'id[]=b&id[]=c' 'repeat' 'id=b&id=c'
    params: Object.assign(Public, data),
    data: Object.assign(Public, data),
    headers: regExpMethod.test(opts.method)
        ? {
          ...defaultHeaders,
          ...{
            'X-CSRF-TOKEN': localStorage.getItem('Xcsrftoken')
          }
        }
      : defaultHeaders,
    cancelToken: new axios.CancelToken(function(c) {
      cancel = c // 记录当前请求的取消方法
    })
  }
  if (opts.method == 'get') {
    delete httpDefaultOpts.data
  } else if (opts.method == 'delete') {
    delete httpDefaultOpts.data
  } else {
    delete httpDefaultOpts.params
    httpDefaultOpts.data = qs.stringify(httpDefaultOpts.data, {
      arrayFormat: httpDefaultOpts.arrayFormat || 'indices'
    })
  }
  let promise = new Promise(function(resolve, reject) {
    axios(httpDefaultOpts)
      .then(res => {
        success(res)
        resolve(res)
      })
      .catch(response => {
        error(response)
        reject(response)
      })
  })
  return promise
}
export default $axios
