import vm from '../../main'

/**
 * 修改下拉数据是否被禁用
 * @param {val， list} val是change方法接受的下拉列表的value，list是包含扫描下拉列表的一系列数据
 * @return 返回处理之后的下拉列表
 */
const changesDropDownListItemC = ({val, list}) => {
  console.log('第一步 通过下拉选择 确定了扫描类型 此时的扫描类型为:', val)
  let options = []
  let arr = list.slice()
  if (val === 'ALL') {
    arr.forEach(ele => {
      if (ele.ruleProp === 'docType') {
        let scanTypeOption = [...ele.option]
        scanTypeOption.forEach(e => {
          if (e.value !== val) {
            e.disabled = true
          }
        })
        ele.option = scanTypeOption
        options = scanTypeOption
      }
    })
  } else {
    arr.forEach(ele => {
      if (ele.ruleProp === 'docType') {
        let scanTypeOption = [...ele.option]
        scanTypeOption[0].disabled = true
        ele.option = scanTypeOption
        options = scanTypeOption
      }
    })
  }
  return arr
}
/**
 * 本地测试的时候返回扫描图像
 */
const responseImgListC = () => {
  return {
    data: {
      flag: true,
      result: new Array(4).fill(1).map((item, idx) => {
        return `${vm.$manifestFolder}/passport/${idx + 1}.jpg`
      })
    }
  }
}
/**
 * 没有选择所有的选项
 */
const isNotAllC = (parm) => { // 没有选择all
  parm.viewerStyle.isTitle = true // 显示扫描图片上方的title
  let isTitle = parm.pageImg.slice()
  const TITLE = isTitle.map(e => {
    return e.docType
  })
  let isExist = TITLE.includes(parm.docType) // 查找是否已经存在title
  let index = TITLE.indexOf(parm.docType) // 查找已经存在title的索引
  let { label } = parm.selectOption.find(item => {
    if (item.value === parm.docType) {
      return item
    }
  })
  if (isExist) { // 如果扫描的图像title已经存在
    parm.pageImg[index].url = [...parm.pageImg[index].url, ...parm.res] // 连接图像数组
    parm.viewerStyle.titleIndex = `${index}` // 将高亮标题放在对应的标题上
  } else { // 若不存在 直接push
    parm.pageImg.push({
      title: label,
      docType: parm.docType,
      url: parm.res
    })
    parm.viewerStyle.titleIndex = `${parm.pageImg.length - 1}` // 将高亮标题放在对应的标题上最后一个
  }
}
/**
 * 选择了所有
 */
const isAllC = (parm) => { // 选择alldisabled: true
  if (parm.pageImg.length === 0) { // 如果图片数组没有数据 则直接push
    parm.pageImg.push({
      title: parm.selectOption[0].label,
      docType: 'ALL',
      url: parm.res
    })
  } else { // 否则连接图像数组
    parm.pageImg[0].url = [...parm.pageImg[0].url, ...parm.res]
  }
  parm.viewerStyle.isTitle = true // 不展示title
  // console.log('确实是点击了all，并且不应该出现表标题 若展示标题的变量isTitle = false 代码没问题', parm.viewerStyle.isTitle)
}
/**
 * 初始化扫描
 */
const initScanImgC = (parm, cb) => {
  let { docType, res, viewerStyle, pageImg, selectOption } = parm
  if (docType !== 'ALL') { // 如果点的是不是all来扫描图片
    let options = {
      docType,
      res,
      viewerStyle,
      pageImg,
      selectOption
    }
    isNotAllC(options)
  } else { // 如果点击的是all来扫描图片
    let opt = { res, pageImg, selectOption, viewerStyle }
    isAllC(opt)
  }
  cb && cb()
  return new Date().getTime() // 记录扫描结束时间
}
/**
 * 恢复扫描类型
 */
const restoreListC = (list) => {
  let arr = list.slice()
  arr.forEach(ele => {
    if (ele.ruleProp === 'docType') {
      let scanTypeOption = [...ele.option]
      scanTypeOption.forEach(e => {
        e.disabled = false
      })
      ele.option = scanTypeOption
      ele.models = 'ALL'
    }
  })
  return arr
}
/**
 * 按照标题删除 title
 */
const clearViewerPhotoC = (options) => {
  let { idx, pageImg, list } = options
  let arr = pageImg.slice()
  arr.splice(idx, 1)
  let len = arr.length
  return {
    arr,
    index: 0,
    len
  }
}
/**
 * 按照标题下的单个删除 thumbnail
 */
const getThumbIndexC = (options) => {
  let { index, pageImg, viewerStyle: { titleIndex } } = options
  let arr = pageImg.slice()
  arr[titleIndex].url.splice(index, 1)
  let len = arr[titleIndex].url.length
  let getArr = {}
  if (!len) {
    getArr = clearViewerPhotoC({ idx: titleIndex, pageImg, list: arr })
  }
  return { arr: Object.keys(getArr).length === 3 ? getArr.arr : arr, len }
}
export {
  changesDropDownListItemC,
  responseImgListC,
  initScanImgC,
  clearViewerPhotoC,
  getThumbIndexC,
  restoreListC
}
