import vm from '../../main'

const verificationRules = (arr) => {
  let rules = {
    licensePlateNumber: (bool = true) => {
      return [ // 前车牌号校验
        { required: bool, message: vm.$t('ViBorder.businessLogicCommon.inputLpn'), trigger: 'blur' },
        { min: 1, max: 20, message: vm.$t('ViBorder.businessLogicCommon.lpnLength'), trigger: 'blur' },
        { pattern: /^[\u4e00-\u9fa5a-zA-Z0-9]+$/, message: vm.$t('ViBorder.businessLogicCommon.onlyLpn') }
      ]
    },
    licensePlateNumberBack: (bool = false) => {
      return [ // 后车牌校验
        { required: bool, message: vm.$t('ViBorder.businessLogicCommon.lpnBackWarn'), trigger: 'blur' },
        { min: 1, max: 20, message: vm.$t('ViBorder.businessLogicCommon.lpnLength'), trigger: 'blur' },
        { pattern: /^[\u4e00-\u9fa5a-zA-Z0-9]+$/, message: vm.$t('ViBorder.businessLogicCommon.onlyLpnBack') }
      ]
    },
    asycudaCode: () => {
      return [ // Asycuda校验
        { min: 1, max: 100, message: vm.$t('ViBorder.CHECKIN.asycudaLengthWarn'), trigger: 'blur' },
        { pattern: /^[a-zA-Z0-9,]+$/, message: vm.$t('ViBorder.businessLogicCommon.onlyNumAndChat') }
      ]
    },
    logistics: (bool = true) => {
      return [ // 物流监管校验
        { required: bool, message: vm.$t('ViBorder.businessLogicCommon.logisticsWarn'), trigger: 'blur' }
      ]
    },
    declarationNumber: () => {
      return [ // 报关单号校验
        { min: 1, max: 100, message: vm.$t('ViBorder.CHECKIN.declarationNumberWarn'), trigger: 'blur' },
        { pattern: /^[a-zA-Z0-9,]+$/, message: vm.$t('ViBorder.businessLogicCommon.onlyNumAndChat') }
      ]
    },
    vehicleType: (bool = true) => {
      return [ // 车辆类型校验
        { required: bool, message: vm.$t('ViBorder.CHECKIN.vehicleTypeWarn'), trigger: 'blur' }
      ]
    },
    xrayScan: (bool = true) => {
      return [ // 扫描图像校验
        { required: bool, message: vm.$t('ViBorder.businessLogicCommon.xrayScanWarn'), trigger: 'blur' }
      ]
    },
    remark: () => {
      return [ // 备注校验
        { min: 1, max: 500, message: vm.$t('ViBorder.businessLogicCommon.RemarkWarn'), trigger: 'blur' }
      ]
    },
    destPortI18N: (bool = false) => {
      return [ // 指运地校验
        { required: bool, message: vm.$t('ViBorder.businessLogicCommon.destinationWarn'), trigger: 'blur' }
      ]
    }
  }
  let obj = {}
  arr.forEach(ele => {
    if (ele.key && typeof rules[ele.key] === 'function') {
      // 定制布尔值
      Object.assign(obj, { [ele.key]: rules[ele.key](ele.bool) })
    } else {
      // 默认
      Object.assign(obj, { [ele]: rules[ele]() })
    }
  })
  // console.log(obj)
  return obj
}

export default verificationRules
