/**
 *  检入
 *  CHECKIN
 *  业务逻辑
 *  BUSINESSLOGIC
 *  数据比对
 *  DATACOMPARISON
 *  测量
 *  MEASUREDMENT
 *  信息中心
 *  IC
 *  手检
 *  PHYSICALINSPECTION
 *  验出放行
 *  CHECKOUT
 *  系统
 *  SYSTEM
 */
let reconnection = {}
// 重连回调函数
let reconCallBack = ''
/**
 * 创建websocket连接并订阅广播消息
 */
let connect = options => {
  reconnection = options.client
  reconCallBack = options.reconCallBack
  let headers = {
    // 开发环境需要
    // 'X-APP-UACCOUNT': 'admin',
    systemName: options.header
  }
  options.client.connect(
    headers,
    function (frame) {
      if (Array.isArray(options.subscribeData)) {
        options.subscribeData.forEach(item => {
          options.client.subscribe(item.path, function (msg) {
            // console.log(JSON.parse(msg.body), '==========================')
            item.scb && item.scb(JSON.parse(msg.body))
          })
        })
      } else if (options.path && typeof options.path === 'string') {
        options.client.subscribe(options.path, function (msg) {
          // console.log(JSON.parse(msg.body), '==========================')
          options.scb && options.scb(JSON.parse(msg.body))
        })
      }
      if (frame && (frame.command === 'CONNECTED' || options.client.connected)) {
        options.callBack && options.callBack()
      }
    },
    onError
  )
}

/**
 * 错误处理
 */

let onError = err => {
  let checkSessionThrough = true
  console.log(typeof err)
  console.log('err', err)
  // if (typeof checkSession === 'function') {
  //   checkSessionThrough = checkSession()
  // }
  setTimeout(() => {
    if (checkSessionThrough) {
      reconCallBack && reconCallBack(reconnection)
    }
  }, 3000)
}

/**
 * 断开websocket连接
 * @returns
 */
let disconnect = options => {
  if (options.client != null) {
    options.client.disconnect()
  }
  // setConnected(false)
  console.log('Disconnected')
}

/**
 * 通过websocket发送消息到controller
 * @returns
 */
let sendMessage = options => {
  console.log('通过websocket发送消息到controller')
  // sendMessage({ client: this.WSClient, path: `/viborderWs/vehicleListRefresh`, parm: { site: localStorage.smsSite } })
  options.client.send(options.path, {}, JSON.stringify(options.parm || {}))
}

export { connect, disconnect, sendMessage }
