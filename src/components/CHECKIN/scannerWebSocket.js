/*******************************************************************************
 * 建立webSocket连接
 ******************************************************************************/
var webSocketScan
function startWebSocketScannerConnect () {
  if (!window.WebSocket) {
    console.log('websocket_alert.notSupport')
    return
  }
  var url = window.config.scannerWebSocket
  webSocketScan = new ReconnectingWebSocket(url)
  webSocketScan.onerror = function (event) {
    onErrorScanner(event)
  }

  webSocketScan.onopen = function (event) {
    onOpenScanner(event)
  }

  webSocketScan.onmessage = function (event) {
    onMessageScanner(event)
  }
}
/**
 * 得到服务器推送消息
 */
function onMessageScanner (event) {
  var jsonObj = JSON.parse(event.data)
  if (jsonObj.error !== '1') {
    // art.dialog.alert(vehicleRegister.scanFailed)
  }
  if (jsonObj.counter > '0') {
    scan(jsonObj.taskId, jsonObj.counter, jsonObj.images)
  }
}

function onOpenScanner (event) {
  if (webSocketScan.readyState !== '1') {
    art.dialog.alert(initAdapter.scanner)
  }
}

function onErrorScanner (event) {
}
/**
 * 这个方法做了一些操作、然后调用回调函数
 */
function doCallbackScanner (fn, args) {
  fn.apply(this, args)
}
export default startWebSocketScannerConnect
