/*******************************************************************************
 * 建立webSocket连接
 ******************************************************************************/
var webSocketRFID
function startWebSocketRFIDConnect () {
  console.log('ok')
  if (!window.WebSocket) {
    // art.dialog.alert(websocket_alert.notSupport)
    return
  }
  var url = window.config.rfidWebSocket
  webSocketRFID = new ReconnectingWebSocket(url)
  webSocketRFID.onerror = function (event) {
    onErrorRFID(event)
  }
  webSocketRFID.onopen = function (event) {
    onOpenRFID(event)
  }
  webSocketRFID.onmessage = function (event) {
    onMessageRFID(event)
  }
  webSocketRFID.onclose = function (event) {
    onCloseRFID(event)
  }
}
/**
 * 得到服务器推送消息
 */
function onMessageRFID (event) {
  var jsonObj = JSON.parse(event.data)
//   $("#checkInInfo\\.rfidNumber").val(jsonObj.cardId)
}

function onOpenRFID (event) {
  if (webSocketRFID.readyState !== '1') {
    art.dialog.alert(initAdapter.rfid)
  }
}

function onErrorRFID (event) {
  console.log(webSocketRFID.readyState)
}

function onCloseRFID (event) {
  console.log(webSocketRFID.readyState)
}
/**
 * 这个方法做了一些操作、然后调用回调函数
 */
function doCallbackRFID (fn, args) {
  fn.apply(this, args)
}

export default startWebSocketRFIDConnect
