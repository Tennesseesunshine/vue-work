const startStopTime = (data) => {
  if (Array.isArray(data) && data.length) {
    if (!data[1].includes('999')) {
      data[1] = `${data[1]}.999`
    }
    return {
      startTime: new Date(data[0]).getTime(),
      endTime: new Date(data[1]).getTime()
    }
  }
}
export {
  startStopTime
}
