/**
 * 绘图使用变量
 */
var x = 1 // 起始坐标X
var y = 1 // 起始坐标Y
var canvas // 图像容器
var ctx // 容器绘制对象（画笔）
var imgDivWidth

/**
 * 处理NII扫描缩略图
 */
function dealNIIimage (jsonObj) {
  // console.log(document.getElementById('con_bt').offsetWidth + '=====' + document.getElementById('con_bt').offsetWidth)
  // console.log('jsonObj', jsonObj)
  if (typeof jsonObj !== 'undefined') {
    var event = jsonObj.status
    // 扫描图像数据传输开始
    if (event === 'thumbnailStart') {
      x = 1
      y = 1
      // 定义画布宽度、高度

      canvas = document.getElementById('can')
      // console.log('canvas', canvas)
      // canvas.width = 1
      canvas.width = document.getElementById('con_bt').clientWidth
      // 高度设定为展示区域的高度
      canvas.height = document.getElementById('con_bt').clientHeight

      ctx = canvas.getContext('2d')
      imgDivWidth = document.getElementById('con_bt').clientWidth
    } else if (event === 'thumbnailFrame') {
      // 接收扫描图像数据传输
      var colorString = jsonObj.colorString
      drawImage(colorString)
    } else if (event === 'thumbnailEnd') {
      // 扫描图像数据传输结束,重置起始坐标
      x = 1
      y = 1
    }
  }
}
/**
 * 绘图[绘制颜色块像素]
 * @param colorString
 * @returns
 */
function drawImage (colorString) {
  var canHeight = document.getElementById('con_bt').clientHeight
  var color = colorString.split(',')
  var scaleVal = canHeight / color.length
  canvas = document.getElementById('can')
  ctx = canvas.getContext('2d')
  ctx.scale(scaleVal, scaleVal)

  for (var i = 0; i < color.length; i++) {
    ctx.fillStyle = '#' + color[i]
    ctx.fillRect(x, y, 1, 1)
    y++
  }

  ctx.scale(1 / scaleVal, 1 / scaleVal)

  if (x * scaleVal >= imgDivWidth - 3) {
    resize(x * scaleVal + 3, document.getElementById('con_bt').clientHeight)
  }

  y = 1
  x++
}

/**
 * 重置画布大小
 * @param w
 * @param h
 */
function resize (w, h) {
  var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height)

  // console.log(canvas.width + '-----' + imgDivWidth + '=========')

  canvas.width = w
  canvas.height = h
  ctx.putImageData(imgData, 0, 0)

  if (canvas.width > imgDivWidth) {
    document.getElementById('con_bt').scrollLeft = canvas.width - imgDivWidth
  }
}

export {
  // 很关键
  dealNIIimage,
  resize
}
