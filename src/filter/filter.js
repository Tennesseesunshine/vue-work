/* eslint-disable */
/**
 * Created by zzg on 2017/12/19.
 */
import Vue from 'vue'
 
Vue.filter('formatDate', function (value, format) {
  if (value === null || value === '' || value === undefined) {
    return
  }
  var paddNum = function (num) {
    num += ''
    return num.replace(/^(\d)$/, '0$1')
  }
  var date = new Date(parseInt(value))
  var cfg = {
    yyyy: date.getFullYear(),
    yy: date.getFullYear().toString().substring(2),
    M: date.getMonth() + 1,
    MM: paddNum(date.getMonth() + 1),
    d: date.getDate(),
    dd: paddNum(date.getDate()),
    HH: paddNum(date.getHours()),
    mm: paddNum(date.getMinutes()),
    ss: paddNum(date.getSeconds())
  }
  return format.replace(/([a-z])(\1)*/ig, function (m) {
    return cfg[m]
  })
})
Vue.filter('FileSize', function (size) {
  var result
  switch (true) {
    case (size === null || size === '' || isNaN(size)):
      result = '-'
      break
    case (size >= 0 && size < 1024):
      result = size + ' B'
      break
    case (size >= 1024 && size < Math.pow(1024, 2)):
      result = Math.round(size / 1024 * 100) / 100 + ' K'
      break
    case (size >= Math.pow(1024, 2) && size < Math.pow(1024, 3)):
      result = Math.round(size / Math.pow(1024, 2) * 100) / 100 + ' M'
      break
    case (size >= Math.pow(1024, 3) && size < Math.pow(1024, 4)):
      result = Math.round(size / Math.pow(1024, 3) * 100) / 100 + ' G'
      break
    default:
      result = Math.round(size / Math.pow(1024, 4) * 100) / 100 + ' T'
  }
  return result
})
Vue.filter('SpecialFont', function (value) {
  return value.replace(/['"\\/\b\f\n\r\t]/g, '').replace(/[-_,!|~`()#$%^&*{}:;"<>?《》""@+.]/g, '').replace('[', '').replace(']', '')
})
/* MD5加密过滤器 */
Vue.filter('GetMD5', function (value, size) {
  return value.MD5(size)
})
/* 字符串反转过滤器 */
Vue.filter('reverse', function (value) {
  return value.split('').reverse().join('')
})
/* 百分比转换过滤器 */
Vue.filter('numToPercent', function (value, digits) {
  var result
  if (digits === null || digits === undefined) {
    digits = 2
  }
  digits = parseInt(digits)
  if (value === null || value === '' || value === undefined || isNaN(value)) {
    result = '-'
  } else {
    result = Math.round(value * Math.pow(10, digits) * 100) / Math.pow(10, digits) + '%'
  }
  return result
})
/* 货币过滤器 */
Vue.filter('formatAmount', function (value, symbol) {
  var result
  if (value === null || value === '' || value === undefined || isNaN(value) || /\D+\.\D+/.test(value)) {
    return '-'
  }
  value = value.replace(/^0*[-+]*0*/, '')
  value = value.replace(/\s/g, '')
  value = value.replace(/^(\d*)$/, '$1.')
  value = (value + '00').replace(/(\d*\.\d\d)\d*/, '$1')
  value = value.replace('.', ',')
  var re = /(\d)(\d{3},)/
  while (re.test(value)) {
    value = value.replace(re, '$1,$2')
  }
  value = value.replace(/,(\d\d)$/, '.$1')
  result = symbol + value.replace(/^\./, '0.')
  return result
})
