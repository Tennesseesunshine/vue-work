import Vue from 'vue'
import Router from 'vue-router'
import handleData from '../api/common'
import store from '../vuex/store'
import vm from '../main'
import { Notification } from 'element-ui'
const elementcss = require('element-ui/lib/theme-chalk/index.css')
Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/menu',
      meta: {
        title: 'ViBorder.systemModule.MAINTITLE.loginTitle'
      },
      component: resolve => require(['@/views/common/menu'], resolve)
    },
    {
      path: `/login`,
      component: resolve => require(['@/views/common/login'], resolve),
      meta: {
        title: 'ViBorder.systemModule.MAINTITLE.loginTitle',
        keepAlive: false
      }
    },
    {
      path: '/language',
      name: 'ViBorder.language',
      component: resolve => require(['@/views/common/language'], resolve)
    },
    {
      KEY: 'CHECKIN',
      path: '/checkIn',
      redirect: '/checkIn/index',
      child: false,
      sortByKey: 1,
      sysIcon: '&#xe63e;',
      meta: {
        title: 'ViBorder.systemModule.CHECKIN.headTitle',
        hasSiteLaneNumber: true,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.CHECKIN',
          path: '/checkIn/index',
          component: resolve => require(['@/views/CHECKIN/checkIn'], resolve)
          // component: resolve => require(['@/views/CHECKIN/checkInxiugai'], resolve)
        },
        {
          name: 'ViBorder.CHECKINDETAIL',
          path: '/checkIn/checkInDetail',
          component: resolve =>
            require(['@/views/CHECKIN/checkInDetail'], resolve)
        }
      ]
    },
    {
      KEY: 'SMS',
      path: '/sms',
      redirect: '/sms/index',
      child: false,
      sortByKey: 2,
      sysIcon: '&#xe648;',
      meta: {
        title: 'ViBorder.systemModule.SMS.headTitle',
        hasSiteLaneNumber: true,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.SMS',
          path: '/sms/index',
          component: resolve => require(['@/views/SMS/index'], resolve)
        },
        {
          name: 'ViBorder.SMS',
          path: '/sms/smsDetail',
          component: resolve => require(['@/views/SMS/smsDetail'], resolve)
        },
        {
          name: 'ViBorder.SMS',
          path: '/sms/smsQuery',
          component: resolve => require(['@/views/SMS/smsQuery'], resolve)
        }
      ]
    },
    {
      KEY: 'AC',
      path: '/ac',
      redirect: '/ac/index',
      child: false,
      sortByKey: 3,
      sysIcon: '&#xe687;',
      meta: {
        title: 'ViBorder.systemModule.AC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          path: '/ac/index',
          component: resolve => require(['@/views/AC/index'], resolve)
        },
        {
          path: '/ac/historicalDetails',
          component: resolve =>
            require(['@/views/AC/historicalDetails'], resolve)
        },
        {
          path: '/ac/check',
          component: resolve => require(['@/views/AC/check'], resolve)
        }
      ]
    },
    {
      KEY: 'PC',
      path: '/pc',
      redirect: '/pc/index',
      child: false,
      sortByKey: 4,
      sysIcon: '&#xe669;',
      meta: {
        title: 'ViBorder.systemModule.PC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          path: '/pc/index',
          component: resolve => require(['@/views/PC/index'], resolve)
        },
        {
          path: '/pc/historicalDetails',
          component: resolve =>
            require(['@/views/PC/historicalDetails'], resolve)
        },
        {
          path: '/pc/check',
          component: resolve => require(['@/views/PC/check'], resolve)
        }
      ]
    },
    {
      KEY: 'HC',
      path: '/hc',
      redirect: '/hc/index',
      child: false,
      sortByKey: 5,
      sysIcon: '&#xe668;',
      meta: {
        title: 'ViBorder.systemModule.HC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          path: '/hc/index',
          component: resolve => require(['@/views/HC/index'], resolve)
        },
        {
          path: '/hc/historicalDetails',
          component: resolve =>
            require(['@/views/HC/historicalDetails'], resolve)
        },
        {
          path: '/hc/check',
          component: resolve => require(['@/views/HC/check'], resolve)
        }
      ]
    },
    {
      KEY: 'TC',
      path: '/tc',
      redirect: '/tc/index',
      child: false,
      sortByKey: 6,
      sysIcon: '&#xe651;',
      meta: {
        title: 'ViBorder.systemModule.TC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.TC',
          path: '/tc/index',
          component: resolve => require(['@/views/TC/index'], resolve)
        },
        {
          name: 'ViBorder.TCOPERATION',
          path: '/tc/tcOperation',
          component: resolve => require(['@/views/TC/tcOperation'], resolve)
        },
        {
          name: 'ViBorder.TCDETAIL',
          path: '/tc/tcDetail',
          component: resolve => require(['@/views/TC/tcDetail'], resolve)
        }
      ]
    },
    {
      KEY: 'JI',
      path: '/ji',
      redirect: '/ji/index',
      child: false,
      sortByKey: 7,
      sysIcon: '&#xe644;',
      meta: {
        title: 'ViBorder.systemModule.JI.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.JI',
          path: '/ji/index',
          component: resolve => require(['@/views/JI/index'], resolve)
        }
      ]
    },
    {
      KEY: 'DC',
      path: '/dc',
      redirect: '/dc/index',
      child: false,
      sortByKey: 8,
      sysIcon: '&#xe797;',
      meta: {
        title: 'ViBorder.systemModule.DC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.DC',
          path: '/dc/index',
          component: resolve => require(['@/views/DC/index'], resolve)
        },
        {
          name: 'ViBorder.DCCHECK',
          path: '/dc/dcCheck',
          component: resolve => require(['@/views/DC/dcCheck'], resolve)
        },
        {
          name: 'ViBorder.DCDETAIL',
          path: '/dc/dcDetail',
          component: resolve => require(['@/views/DC/dcDetail'], resolve)
        }
      ]
    },
    {
      KEY: 'PI',
      path: '/pi',
      redirect: '/pi/index',
      child: false,
      sortByKey: 9,
      sysIcon: '&#xe646;',
      meta: {
        title: 'ViBorder.systemModule.PI.headTitle',
        hasSiteLaneNumber: true,
        onlySite: true
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.PIINDEX',
          path: '/pi/index',
          component: resolve => require(['@/views/PI/index'], resolve)
        },
        {
          name: 'ViBorder.PI',
          path: '/pi/piDetailTunnel',
          component: resolve => require(['@/views/PI/piDetailTunnel'], resolve)
        },
        {
          name: 'ViBorder.PINOTUNNEL',
          path: '/pi/piOperationTunnel',
          component: resolve =>
            require(['@/views/PI/piOperationTunnel'], resolve)
        }
      ]
    },
    {
      KEY: 'CI',
      path: '/ci',
      redirect: '/ci/index',
      child: false,
      sortByKey: 10,
      sysIcon: '&#xe642;',
      meta: {
        title: 'ViBorder.systemModule.CI.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          path: '/ci/index',
          component: resolve =>
            require(['@/views/CUSTOMSINQUIRY/index'], resolve)
        },
        {
          path: '/ci/ciDetail',
          component: resolve =>
            require(['@/views/CUSTOMSINQUIRY/ciDetail'], resolve)
        }
      ]
    },
    {
      KEY: 'RC',
      path: '/rc',
      redirect: '/rc/index',
      child: false,
      sortByKey: 10,
      sysIcon: '&#xe642;',
      meta: {
        title: 'ViBorder.systemModule.RC.headTitle',
        hasSiteLaneNumber: true,
        onlySite: true
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.RC',
          path: '/rc/index',
          component: resolve => require(['@/views/RC/index'], resolve)
        }
      ]
    },
    {
      KEY: 'CHECKOUT',
      path: '/checkOut',
      redirect: '/checkOut/index',
      child: false,
      sortByKey: 11,
      sysIcon: '&#xe645;',
      meta: {
        title: 'ViBorder.systemModule.CHECKOUT.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.CHECKOUT',
          path: '/checkOut/index',
          component: resolve => require(['@/views/CHECKOUT/index'], resolve)
        }
      ]
    },
    {
      KEY: 'MR',
      path: '/mr',
      redirect: '/mr/index',
      child: false,
      sortByKey: 12,
      sysIcon: '&#xe643;',
      meta: {
        title: 'ViBorder.systemModule.MR.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.MR',
          path: '/mr/index',
          component: resolve => require(['@/views/MR/index'], resolve)
        },
        {
          name: 'ViBorder.MRDETAIL',
          path: '/mr/mrDetail',
          component: resolve => require(['@/views/MR/mrDetail'], resolve)
        }
      ]
    },
    {
      KEY: 'IC',
      path: '/ic/home',
      prmKey: 'home',
      child: false,
      icon: '&#xe616;',
      sysIcon: '&#xe641;',
      content: 'ViBorder.common.homePage',
      meta: {
        title: 'ViBorder.systemModule.IC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.IC.welcome',
          path: '/ic/home',
          component: resolve => require(['@/views/IC/home'], resolve)
        }
      ]
    },
    {
      KEY: 'IC',
      path: '/ic/portMonitor',
      prmKey: 'VIBORDER-IC-PORTMONITOR',
      child: false,
      icon: '&#xe616;',
      content: 'ViBorder.IC.monitoringOverview',
      meta: {
        title: 'ViBorder.systemModule.IC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.IC.siteMonitoring',
          path: '/ic/portMonitor',
          component: resolve => require(['@/views/IC/index'], resolve)
        }
      ]
    },
    {
      KEY: 'IC',
      path: '/ic/taskRegulatory',
      child: false, // 是否有子路由
      icon: '&#xe77f;', // 占位图标
      prmKey: 'VIBORDER-IC-REGULTATORY',
      content: 'ViBorder.IC.taskRegulatory',
      meta: {
        title: 'ViBorder.systemModule.IC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.IC.taskRegulatory',
          path: '/ic/taskRegulatory',
          component: resolve => require(['@/views/IC/taskRegulatory'], resolve)
        }
      ]
    },
    {
      KEY: 'IC',
      path: '/ic/businessAudit',
      prmKey: 'VIBORDER-IC-AUDIT',
      child: true,
      icon: '&#xe63c;',
      meta: {
        title: 'ViBorder.systemModule.IC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.IC.businessAudit',
          path: '/ic/businessAudit',
          component: resolve => require(['@/views/IC/businessAudit'], resolve)
        },
        {
          name: 'ViBorder.IC.businessVerification',
          path: '/ic/businessVerification',
          component: resolve =>
            require(['@/views/IC/businessVerification'], resolve)
        }
      ]
    },
    {
      KEY: 'IC',
      path: '/ic/businessStatistics',
      prmKey: 'VIBORDER-IC-STATISTICS',
      child: true,
      icon: '&#xe67f;',
      meta: {
        title: 'ViBorder.systemModule.IC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.IC.businessStatisticsQuery',
          path: '/ic/businessStatisticsQuery',
          component: resolve =>
            require(['@/views/IC/businessStatisticsQuery'], resolve)
        },
        {
          name: 'ViBorder.IC.businessStatisticsReport',
          path: '/ic/businessStatisticsReport',
          component: resolve =>
            require(['@/views/IC/businessStatisticsReport'], resolve)
        },
        {
          name: 'ViBorder.IC.importBusinessReport',
          path: '/ic/importBusinessReport',
          component: resolve =>
            require(['@/views/IC/importBusinessReport'], resolve)
        },
        {
          name: 'ViBorder.IC.exitBusinessReport',
          path: '/ic/exitBusinessReport',
          component: resolve =>
            require(['@/views/IC/exitBusinessReport'], resolve)
        },
        {
          name: 'ViBorder.IC.animalReport',
          path: '/ic/animalReport',
          component: resolve =>
            require(['@/views/IC/animalReport'], resolve)
        },
        {
          name: 'ViBorder.IC.plantReport',
          path: '/ic/plantReport',
          component: resolve =>
            require(['@/views/IC/plantReport'], resolve)
        },
        {
          name: 'ViBorder.IC.healthReport',
          path: '/ic/healthReport',
          component: resolve =>
            require(['@/views/IC/healthReport'], resolve)
        },
        {
          name: 'ViBorder.IC.trafficReport',
          path: '/ic/trafficReport',
          component: resolve =>
            require(['@/views/IC/trafficReport'], resolve)
        },
        {
          name: 'ViBorder.IC.vehicleTransactionStatusReport',
          path: '/ic/vehicleTransactionStatusReport',
          component: resolve =>
            require(['@/views/IC/vehicleTransactionStatusReport'], resolve)
        },
        {
          name: 'ViBorder.IC.statisticalReportSettings',
          path: '/ic/statisticalReportSettings',
          component: resolve =>
            require(['@/views/IC/statisticalReportSettings'], resolve)
        }
      ]
    },
    {
      KEY: 'IC',
      path: '/ic/whitelistCarQuery',
      prmKey: 'VIBORDER-IC-WHITELIST',
      child: true,
      icon: '&#xe79a;',
      content: 'ViBorder.IC.whitelistCarQuery',
      meta: {
        title: 'ViBorder.systemModule.IC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.IC.whitelistCarQuery',
          path: '/ic/whitelistCarQuery',
          component: resolve =>
            require(['@/views/IC/whitelistCarQuery'], resolve)
        },
        {
          name: 'ViBorder.IC.whitelistManagement',
          path: '/ic/whitelistManagement',
          component: resolve =>
            require(['@/views/IC/whitelistManagement'], resolve)
        }
      ]
    },
    {
      KEY: 'IC',
      path: '/ic/vehicleManagement',
      prmKey: 'VIBORDER-IC-VEHICLEMANAGER',
      child: true,
      icon: '&#xe628',
      meta: {
        title: 'ViBorder.systemModule.IC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.IC.vehicleInspection',
          path: '/ic/vehicleInspectionQuery',
          component: resolve =>
            require(['@/views/IC/vehicleInspectionQuery'], resolve)
        },
        {
          name: 'ViBorder.IC.vehicleMeasured',
          path: '/ic/measureVehicle',
          component: resolve => require(['@/views/IC/measureVehicle'], resolve)
        },
        {
          name: 'ViBorder.IC.approchingVehicleQuery',
          path: '/ic/approachingVehicle',
          component: resolve =>
            require(['@/views/IC/approachingVehicle'], resolve)
        }
      ]
    },
    {
      KEY: 'IC',
      path: '/ic/businessTime',
      prmKey: 'VIBORDER-IC-TIMING',
      child: true,
      icon: '&#xe810',
      content: 'ViBorder.IC.businessTiming',
      meta: {
        title: 'ViBorder.systemModule.IC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.IC.businessTiming',
          path: '/ic/businessTiming',
          component: resolve => require(['@/views/IC/businessTiming'], resolve)
        }
      ]
    },
    {
      KEY: 'IC',
      path: '/ic/dataCheck',
      prmKey: 'VIBORDER-IC-CONFIG',
      child: true,
      icon: '&#xe608',
      meta: {
        title: 'ViBorder.systemModule.IC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.IC.dataCheckManagement',
          path: '/ic/dataCheckManagement',
          component: resolve =>
            require(['@/views/IC/dataCheckManagement'], resolve)
        },
        {
          name: 'ViBorder.IC.businessSegmenManagement',
          path: '/ic/businessSegmenManagement',
          component: resolve =>
            require(['@/views/IC/businessSegmenManagement'], resolve)
        }
      ]
    },
    {
      KEY: 'IC',
      path: '/ic/query',
      prmKey: 'VIBORDER-IC-SYSMANE',
      child: true,
      icon: '&#xe65c',
      meta: {
        title: 'ViBorder.systemModule.IC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.IC.eventQuery',
          path: '/ic/operationAuditPage',
          component: resolve =>
            require(['@/views/IC/operationAuditPage'], resolve)
        },
        {
          name: 'ViBorder.IC.commandQuery',
          path: '/ic/commandQuery',
          component: resolve => require(['@/views/IC/commandQuery'], resolve)
        },
        {
          name: 'ViBorder.IC.vehicleInspectionQuery',
          path: '/ic/vehicleEventQuery',
          component: resolve =>
            require(['@/views/IC/vehicleEventQuery'], resolve)
        },
        {
          name: 'ViBorder.IC.deviceStatusQuery',
          path: '/ic/deviceStatusQuery',
          component: resolve =>
            require(['@/views/IC/deviceStatusQuery'], resolve)
        },
        {
          name: 'ViBorder.IC.logTransferQuery',
          path: '/ic/logTransferQuery',
          component: resolve =>
            require(['@/views/IC/logTransferQuery'], resolve)
        }
      ]
    },
    {
      KEY: 'IC',
      path: '/ic/locale',
      prmKey: 'VIBORDER-IC-AUDITLOG',
      child: false,
      icon: '&#xe613;',
      content: 'ViBorder.auditLog.title',
      meta: {
        title: 'ViBorder.systemModule.IC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: 'ViBorder.auditLog.title',
          path: '/ic/locale',
          component: resolve => require(['@/views/IC/locale'], resolve)
        }
      ]
    },
    {
      name: 'ICChildRouter',
      path: '',
      meta: {
        title: 'ViBorder.systemModule.IC.headTitle',
        hasSiteLaneNumber: false,
        onlySite: false
      },
      component: resolve => require(['@/views/common/index'], resolve),
      children: [
        {
          name: '',
          path: '/ic/vehicleInspectionQueryDetail',
          component: resolve =>
            require(['@/views/IC/vehicleInspectionQueryDetail'], resolve)
        },
        // {
        //   name: '',
        //   path: '/ic/vehicleNoTunnelDetail',
        //   component: resolve =>
        //     require(['@/views/IC/vehicleNoTunnelDetail'], resolve)
        // },
        {
          name: 'ViBorder.IC.approchingVehicleQueryDetail',
          path: '/ic/approachingVehicleDetail',
          component: resolve =>
            require(['@/views/IC/approachingVehicleDetail'], resolve)
        },
        // {
        //   name: 'ViBorder.IC.approchingVehicleQueryDetail',
        //   path: '/ic/approachingNoTunnelDetail',
        //   component: resolve =>
        //     require(['@/views/IC/approachingNoTunnelDetail'], resolve)
        // },
        {
          name: '',
          path: '/ic/vehicleEventQueryDetail',
          component: resolve =>
            require(['@/views/IC/vehicleEventQueryDetail'], resolve)
        },
        {
          name: 'ViBorder.IC.businessVerificationDetail',
          path: '/ic/businessVerificationDetail',
          component: resolve =>
            require(['@/views/IC/businessVerificationDetail'], resolve)
        },
        {
          name: 'ViBorder.IC.measurementRecordDetails',
          path: '/ic/measurementRecordDetails',
          component: resolve =>
            require(['@/views/IC/measurementRecordDetails'], resolve)
        },
        {
          name: 'ViBorder.IC.measurementRecordDetails',
          path: '/ic/vehicleInspMeasureRecDet',
          component: resolve =>
            require(['@/views/IC/vehicleInspMeasureRecDet'], resolve)
        },
        // {
        //   name: 'ViBorder.IC.businessVerificationDetail',
        //   path: '/ic/businessVerificationNoTunnelDetail',
        //   component: resolve =>
        //     require(['@/views/IC/businessVerificationNoTunnelDetail'], resolve)
        // },
        {
          name: 'ViBorder.IC.businessAuditDetail',
          path: '/ic/businessAuditDetail',
          component: resolve =>
            require(['@/views/IC/businessAuditDetail'], resolve)
        },
        {
          name: 'ViBorder.IC.businessTimingDetail',
          path: '/ic/businessTimingDetail',
          component: resolve =>
            require(['@/views/IC/businessTimingDetail'], resolve)
        },
        {
          name: 'ViBorder.IC.taskRegulatoryDetail',
          path: '/ic/taskRegulatoryDetail',
          component: resolve =>
            require(['@/views/IC/taskRegulatoryDetail'], resolve)
        },
        {
          name: 'ViBorder.IC.vehicleReportDetails',
          path: '/ic/vehicleReportDetails',
          component: resolve =>
            require(['@/views/IC/vehicleReportDetails'], resolve)
        }
      ]
    },
    {
      path: '*',
      name: 'ViBorder.error',
      component: resolve => require(['@/views/common/error'], resolve)
    }
  ]
})

let add = (() => {
  let counter = 0
  return () => {
    return (counter += 1)
  }
})()
let arr = ['dcCheck', 'dcDetail', 'piDetailTunnel', 'piOperationTunnel'] // vprobe需要存储的路径
router.beforeEach((to, from, next) => {
  // console.log('to', to.path)
  // console.log('from', from.path)
  let toPath = to.path
  let fromPath = from.path
  let need2SavePath = toPath.split('/')[2]
  if (arr.includes(need2SavePath)) {
    store.commit({
      type: 'savePagePath',
      need2SavePath
    })
  }
  // 最后一次登录系统路径
  let lastSys = localStorage.lastLoginSystem
  // 是否为信息中心主页和监控总览
  let isWelcomeHome = localStorage.home
  let path = ''
  if (isWelcomeHome === 'true') {
    path = '/ic/home'
  } else if (isWelcomeHome === 'false') {
    path = '/ic/portMonitor'
  } else {
    path = lastSys ? `/${lastSys}/index` : `/menu`
  }
  let nowPath = document.location.hash.split('/')[1]
  // 先去请求权限接口
  // 有权限的时候
  //   判断权限列表里是否包括当前页面的权限
  //   包括则不作处理
  //   不包括 返回九宫格
  // 无权限 退出用户
  // 判断是否登录
  if (fromPath === '/' && toPath === '/login') {
    handleData
      .chooseLogin()
      .then(res => {
        if (res.data.flag) {
          next({ path: path })
          sessionStorage.setItem('account', res.data.result.account)
          sessionStorage.setItem('userName', res.data.result.userName)
        } else {
          next()
          sessionStorage.removeItem('account')
          sessionStorage.removeItem('userName')
        }
      })
      .catch(err => {
        // if (err.message === 'cancel') {
        //   next({ path: '/login' })
        // }
        next()
        console.log('err', err)
      })
  } else {
    if (toPath === '/language' && from.path === '/') {
      next({ path: '/login' })
    }
    let account = sessionStorage.getItem('account')
    let userName = sessionStorage.getItem('userName')
    if (
      !account &&
      !userName &&
      toPath !== '/login' &&
      toPath !== '/language' &&
      from.path !== '/'
    ) {
      next({ path: '/login' })
    } else {
      // console.log('基本路由都需要走这里')
      next()
    }
    // handleData
    //   .chooseLogin()
    //   .then(res => {
    //     if (res.data.flag) {
    //       sessionStorage.setItem('account', res.data.result.account)
    //       sessionStorage.setItem('userName', res.data.result.userName)
    //       sessionStorage.setItem('userId', res.data.result.userId)
    //       handleData
    //         .getPrmList({ all: 'true', prmCode: `VIBORDER` })
    //         .then(response => {
    //           let prmList = response.data.result
    //           // prmList = [...prmList, ...['VIBORDER-AC', 'VIBORDER-PC', 'VIBORDER-HC', 'VIBORDER-CI']]
    //           if (prmList.length) {
    //             if (
    //               prmList.includes(`VIBORDER-${nowPath.toUpperCase()}`) ||
    //               nowPath === 'menu' ||
    //               nowPath === 'login' ||
    //               nowPath === '' ||
    //               nowPath === 'language'
    //             ) {
    //               // next()
    //             } else {
    //               // localStorage.removeItem('lastLoginSystem')
    //               next({ path: `/menu` })
    //               // 只有当在系统的里面此时被去掉当前系统权限退出到九宫格之后才提示
    //               // if (
    //               //   (from.path !== '/login' || from.path !== '/') &&
    //               //   toPath !== '/menu'
    //               // ) {
    //               //   Notification.info({
    //               //     title: vm.$t('ViBorder.common.prompt'),
    //               //     message: vm.$t('ViBorder.upms.ajax.I010400'),
    //               //     offset: vm.notifyOffset
    //               //   })
    //               // }
    //             }
    //           } else {
    //             // 所有权限不存在的情况
    //             handleData
    //               .logout()
    //               .then(item => {
    //                 if (item.data.flag) {
    //                   let lang = localStorage.lang
    //                   sessionStorage.clear()
    //                   localStorage.setItem('lang', lang)
    //                   next({ path: `/` })
    //                   Notification.info({
    //                     title: vm.$t('ViBorder.common.prompt'),
    //                     message: vm.$t('ViBorder.upms.ajax.E020003'),
    //                     offset: vm.notifyOffset
    //                   })
    //                 }
    //               })
    //               .catch(() => {})
    //           }
    //         })
    //         .catch(error => {
    //           console.log('error', error)
    //         })
    //     } else {
    //       if (toPath === '/login') {
    //         next()
    //       } else {
    //         if (toPath === '/language' && from.path === '/login') {
    //           next({ path: '/login' })
    //         } else {
    //           next(false)
    //         }
    //       }
    //     }
    //   })
    //   .catch(err => {
    //     console.log('err', err)
    //   })
  }
  // if (fromPath === '/' && toPath.split('/').length > 2) {
  //   let count = add()
  //   console.log(count)
  //   if (count !== 1) {
  //     count = 1
  //   }
  //   if (count === 1) {
  //     handleData.getPrmList({ all: 'true', prmCode: `VIBORDER` }).then(response => {
  //       if (!response.data.flag && response.data.errorCode) {
  //         next({ path: '/login' })
  //       }
  //     })
  //   }
  // }
  // console.log('to', to)
  // console.log('from', from)
})

export default router
