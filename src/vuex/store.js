import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

// 共享数据
const state = {
  lang: localStorage.lang || window.config.initLang, // 初始语言
  path: '', // 国际化切换路由
  indexModal: false,
  isShowWelcome: '', // 用于比对站的推送按钮
  audio: '', // 用于测量站的警报灯
  isViProbePathArray: [], // 会打开viprobe的页面的路径数组
  isReleaseCloseBtn: false,
  addNewSearchCondition: false, // 报表查询增加查询条件
  searchConditionOption: {} // 缓存查询条件
}
// 改变数据的方法
// 方法名是vue文件里面commit的type state为上面的数据 parm为vue文件传入的数据参数
const mutations = {
  changeLang (state, parm) {
    state.lang = parm.lang
    state.path = parm.path
  },
  showIndexModal (state, parm) {
    state.indexModal = parm.indexModal
  },
  dcPushButton (state, parm) {
    state.isShowWelcome = parm.isShowWelcome
  },
  audioAndLight (state, parm) {
    state.audio = parm.audio
  },
  savePagePath (state, parm) {
    state.isViProbePathArray.push(parm.need2SavePath)
  },
  releaseBtn (state, parm) {
    state.isReleaseCloseBtn = parm.releaseCloseBtn
  },
  getNewSearchCondition (state, parm) {
    state.addNewSearchCondition = parm.addNewSearchCondition
  },
  cacheSearchCondition (state, parm) {
    state.searchConditionOption = {
      ...state.searchConditionOption,
      ...parm.searchConditionOption
    }
  }
}
// 实例
const store = new Vuex.Store({
  state,
  mutations
})
export default store
