import $axios from '../../components/common/axios'
const VIBORDER = window.config.projectName
export default {
  _initData (data) {
    // 主页查询
    const service = {
      method: 'post',
      url: `${VIBORDER}/ci/listWaitRcVehicle`
    }
    return $axios(service, data)
  },
  _openTask (data) {
    // 跳转详情
    const service = {
      method: 'post',
      url: `${VIBORDER}/ci/openTask`
    }
    return $axios(service, data)
  },
  _initBusinessTypeList (data) {
    // 初始化页面
    const service = {
      method: 'get',
      url: `${VIBORDER}/ci/initBusinessTypeList`
    }
    return $axios(service, data)
  }
}
