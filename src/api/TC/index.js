import $axios from '../../components/common/axios'
const VIBORDER = window.config.projectName
export default {
  _listWaitTrafficConfirm (data) {
    // 主页查询交通确认列表
    const service = {
      method: 'post',
      url: `${VIBORDER}/trafficConfirm/listWaitTrafficConfirm`
    }
    return $axios(service, data)
  },
  _findDetail (data) {
    // 查看交通确认详情
    const service = {
      method: 'get',
      url: `${VIBORDER}/trafficConfirm/findDetail`
    }
    return $axios(service, data)
  },
  _findHisDetail (data) {
    // 查看交通确认详情
    const service = {
      method: 'get',
      url: `${VIBORDER}/trafficConfirm/findAllDetail`
    }
    return $axios(service, data)
  },
  _confirmRelease (data) {
    // 提交确认的方法
    const service = {
      method: 'post',
      url: `${VIBORDER}/trafficConfirm/confirmRelease`
    }
    return $axios(service, data)
  },
  _listTrafficConfirmHis (data) {
    // 侧拉框历史查询交通确认列表
    const service = {
      method: 'post',
      url: `${VIBORDER}/trafficConfirm/listAll`
    }
    return $axios(service, data)
  },
  _createManualMeasure (data) {
    // 人工测量接口
    const service = {
      method: 'post',
      url: `${VIBORDER}/trafficConfirm/createManualMeasure`
    }
    return $axios(service, data)
  },
  _updateManualMeasure (data) {
    // 人工测量的通过和保存的接口
    const service = {
      method: 'post',
      url: `${VIBORDER}/trafficConfirm/updateManualMeasure`
    }
    return $axios(service, data)
  },
  _initManualMeasure (data) {
    // 人工测量的下拉列表数据
    const service = {
      method: 'post',
      url: `${VIBORDER}/trafficConfirm/initManualMeasure`
    }
    return $axios(service, data)
  },
  _queryManualMeasure (data) {
    // 获取人工测量数据
    const service = {
      method: 'post',
      url: `${VIBORDER}/trafficConfirm/queryManualMeasure`
    }
    return $axios(service, data)
  },
  _isAllowOpen (data) {
    // 是否允许进入任务
    const service = {
      method: 'post',
      url: `${VIBORDER}/trafficConfirm/isAllowOpen`
    }
    return $axios(service, data)
  },
  // 下拉列表
  _getDropDownlist (data) {
    const service = {
      method: `get`,
      url: `${VIBORDER}/trafficConfirm/initData`
    }
    return $axios(service, data)
  },
  _listPiFiles (data) {
    // 点击上传文件按钮时调用的，初始化已上传文件的接口
    const service = {
      method: 'post',
      url: `${VIBORDER}/pi/listPiFiles`
    }
    return $axios(service, data)
  },
  // 导出Excel
  _exportExcel (data) {
    const service = {
      method: `post`,
      url: `${VIBORDER}/exportFile/transport/export`
    }
    return $axios(service, data, 'arraybuffer')
  }
}
