import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  // 下拉列表
  _getDropDownlist (data, urlStr) {
    const service = {
      method: `get`,
      url: `${URLPREFIX}/${urlStr}Check/initData`
    }
    return $axios(service, data)
  },
  // 主页查询结果
  _indexList (data, urlStr) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/${urlStr}Check/list`
    }
    return $axios(service, data)
  },
  // 主页列表进入详情判断
  _isAllowOpen (data, urlStr) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/${urlStr}Check/isAllowOpen`
    }
    return $axios(service, data)
  },
  // 在检详情
  _getDetails (data, urlStr) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/${urlStr}Check/findDetail`
    }
    return $axios(service, data)
  },
  // 在检提交
  _submitRemarkInfo (data, urlStr) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/${urlStr}Check/confirmRelease`
    }
    return $axios(service, data)
  },
  // 历史列表
  _listHis (data, urlStr) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/${urlStr}Check/listAll`
    }
    return $axios(service, data)
  },
  // 历史列表详情
  _findHisDetail (data, urlStr) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/${urlStr}Check/findAllDetail`
    }
    return $axios(service, data)
  },
  // 导出Excel
  _exportExcel (data, urlStr) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/exportFile/${urlStr}/export`
    }
    return $axios(service, data, 'arraybuffer')
  },
  // 初始化请求文件
  _listAcFiles (data, urlStr) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/${urlStr}Check/listAcFiles`
    }
    return $axios(service, data)
  },
  // 删除上传文件
  _deleteAcFiles (data, urlStr) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/${urlStr}Check/deleteAcFiles`
    }
    return $axios(service, data)
  },
  // 上传文件
  _uploadAcFiles (data, urlStr) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/${urlStr}Check/uploadAcFiles`
    }
    return $axios(service, data)
  }
}
