import $axios from '../../components/common/axios'
const VIBORDER = window.config.projectName
export default {
  _asycudaQuery (data) { // 确认按钮
    const service = {
      method: 'post',
      url: `${VIBORDER}/jointInspection/confirmJointInspection`
    }
    return $axios(service, data)
  },
  _initList (data) { // 主页列表查询
    const service = {
      method: 'post',
      url: `${VIBORDER}/jointInspection/listWaitJointInspection`
    }
    return $axios(service, data)
  },
  _listJointInspection (data) { // 侧拉框的列表查询
    const service = {
      method: 'post',
      url: `${VIBORDER}/jointInspection/listJointInspectionHis`
    }
    return $axios(service, data)
  }
}
