import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName
export default {
  _initBarChart (data) {
    const service = { // 年度进出境量
      method: 'get',
      url: `${URLPREFIX}/ic/portMonitor/barChartImportExportAmount`
    }
    return $axios(service, data)
  },
  _initPieChart (data) { // 年度进境业务分析
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/portMonitor/pieChartImportBusinessType`
    }
    return $axios(service, data)
  },
  _initStatist (data) { // 页面右面内容接口
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/portMonitor/statisticsData`
    }
    return $axios(service, data)
  },
  _initDevice (data) { // 设备预警
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/portMonitor/deviceAlarms`
    }
    return $axios(service, data)
  },
  _initVehicle (data) { // 超时车辆预警
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/portMonitor/vehicleAlarms`
    }
    return $axios(service, data)
  },
  _initSites (data) { // 区域配置
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/portMonitor/getSiteConfigs`
    }
    return $axios(service, data)
  },
  _initSitesFlow (data) { // 区域配置
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/portMonitor/siteFlowData`
    }
    return $axios(service, data)
  }
}
