import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName
export default {
  _initData (data) {
    const service = { // 初始化下拉数据
      method: 'get',
      url: `${URLPREFIX}/ic/common/initData`
    }
    return $axios(service, data)
  },
  _getReport (data) {
    const service = { // 报表统计
      method: 'post',
      url: `${URLPREFIX}/ic/statistics/joinCount`
    }
    return $axios(service, data)
  },
  _getList (data) {
    const service = { // 列表数据
      method: 'post',
      url: `${URLPREFIX}/ic/statistics/joinList`
    }
    return $axios(service, data)
  },
  _exportFile (data) {
    const service = {
      method: 'post',
      url: `${URLPREFIX}/exportFile/joinList/export`
    }
    return $axios(service, data, 'arraybuffer')
  }
}
