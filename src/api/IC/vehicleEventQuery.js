import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initDropDownList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/common/dictionary`
    }
    return $axios(service, data)
  },
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/vehicleEvent/list`
    }
    return $axios(service, data)
  },
  _initListHistory (data) {
    // 获取表格列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/vehicleEvent/listHistory`
    }
    return $axios(service, data)
  },
  _auditDetailEv (data) {
    // 获取详情页数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/vehicleEvent/vehicleEvents`
    }
    return $axios(service, data)
  },
  _auditDetailHistoryEv (data) {
    // 获取详情页数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/vehicleEvent/vehicleEventsHistory`
    }
    return $axios(service, data)
  },
  _auditDetailCo (data) {
    // 获取详情页数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/vehicleEvent/vehicleCommand`
    }
    return $axios(service, data)
  },
  _auditDetailHistoryCo (data) {
    // 获取详情页数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/vehicleEvent/vehicleCommandHistory`
    }
    return $axios(service, data)
  }
}
