import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initReportName (data) {
    // 获取报表类型
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/httpService/getUserSearch`
    }
    return $axios(service, data)
  },
  _customCount (data) {
    // 统计
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/httpService/customCount`
    }
    return $axios(service, data)
  },
  _initList (data) {
    // 列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/httpService/search`
    }
    return $axios(service, data)
  },
  _initData (data) {
    // 下拉数据
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/httpService/initData`
    }
    return $axios(service, data)
  },
  _getSearchCondation (data) {
    // 下拉数据改变获取有哪些查询条件
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/httpService/getSearchCondation`
    }
    return $axios(service, data)
  },
  _getUpmsUser (data) {
    // 获取员
    const service = {
      method: 'get',
      url: `/userPrm/findUsersByPrmCode`
    }
    return $axios(service, data)
  },
  _exportFile (data) {
    const service = { // 列表数据
      method: 'post',
      url: `${URLPREFIX}/exportFile/transportStatus/export`
    }
    return $axios(service, data, 'arraybuffer')
  },
  _findDetail (data) {
    const service = { // 列表数据
      method: 'post',
      url: `${URLPREFIX}/ic/httpService/findDetail`
    }
    return $axios(service, data)
  }
}
