import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName
export default {
  _getSearchCondation (data) {
    const service = { // 获取所有可选项
      method: 'post',
      url: `${URLPREFIX}/ic/httpService/getSearchCondation`
    }
    return $axios(service, data)
  },
  _getUserSearch (data) {
    const service = { // 获取用户增加的list
      method: 'post',
      url: `${URLPREFIX}/ic/httpService/getUserSearch`
    }
    return $axios(service, data)
  },
  _addUserSearch (data) {
    const service = { // 增加选择项目 参数strSearchCondations, ufType: 1, ufTypeName
      method: 'post',
      url: `${URLPREFIX}/ic/httpService/addUserSearch`
    }
    return $axios(service, data)
  },
  _delUserSearch (data) {
    const service = { // 删除列表ids字符串参数 列表id
      method: 'post',
      url: `${URLPREFIX}/ic/httpService/delUserSearch`
    }
    return $axios(service, data)
  }
}
