import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/logTransfer/list`
    }
    return $axios(service, data)
  }
}
