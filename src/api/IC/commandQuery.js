import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initDropDownList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/common/dictionary`
    }
    return $axios(service, data)
  },
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/command/list`
    }
    return $axios(service, data)
  }
}
