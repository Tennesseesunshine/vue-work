import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initDropDownList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/common/dictionary`
    }
    return $axios(service, data)
  },
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/businessAudit/list`
    }
    return $axios(service, data)
  },
  _initListHistory (data) {
    // 获取表格列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/businessAudit/listHistory`
    }
    return $axios(service, data)
  },
  _auditDetail (data) {
    // 获取详情页数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/businessAudit/auditDetail`
    }
    return $axios(service, data)
  },
  _auditDetailHistory (data) {
    // 获取详情页数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/businessAudit/auditDetailHis`
    }
    return $axios(service, data)
  },
  // 导出Excel
  _exportExcel (data) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/exportFile/businessAudit/export`
    }
    return $axios(service, data, 'arraybuffer')
  }
}
