import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initDropDownList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/common/dictionary`
    }
    return $axios(service, data)
  },
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/businessVerification/list`
    }
    return $axios(service, data)
  },
  _openHisPiTask (data) {
    // 获取详情页数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/businessVerification/vehicleDetailHis`
    }
    return $axios(service, data)
  },
  _businessTiming (data) {
    // 时间轴接口
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/businessTiming/businessOverviewHis`
    }
    return $axios(service, data)
  },
  _getIsExists (data) {
    const service = {
      method: 'get',
      url: `${window.config.Vitracking.commonUrl}/supply/exists-serialnumber`
    }
    return $axios(service, data)
  },
  _findHisDetail (data) {
    // 查看交通确认详情
    const service = {
      method: 'get',
      url: `${URLPREFIX}/trafficConfirm/findHisDetail`
    }
    return $axios(service, data)
  },
  _measureRecords (data) {
    // 查看交通确认详情
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/businessVerification/measureRecords`
    }
    return $axios(service, data)
  },
  _queryManualMeasure (data) {
    // 获取人工测量数据
    const service = {
      method: 'post',
      url: `${URLPREFIX}/trafficConfirm/queryManualMeasure`
    }
    return $axios(service, data)
  },
  // 导出Excel
  _exportExcel (data) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/exportFile/businessVerification/export`
    }
    return $axios(service, data, 'arraybuffer')
  }
}
