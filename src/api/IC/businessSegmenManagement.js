import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initDropDownList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/businessStep/getStepTypes`
    }
    return $axios(service, data)
  },
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/businessStep/list`
    }
    return $axios(service, data)
  },
  _initAdd (data) {
    // 添加表单
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/businessStep/add`
    }
    return $axios(service, data)
  },
  _initModify (data) {
    // 修改表单
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/businessStep/update`
    }
    return $axios(service, data)
  },
  _initDelete (data) {
    // 删除表格数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/businessStep/delete`
    }
    return $axios(service, data)
  },
  _initOnly (data) {
    // 获取唯一数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/businessStep/getById`
    }
    return $axios(service, data)
  },
  _businessStepExoprt (data) {
    // 业务环节阀值管理导出
    const service = {
      method: 'post',
      url: `${URLPREFIX}/exportFile/businessStep/export`
    }
    return $axios(service, data, 'arraybuffer')
  }
}
