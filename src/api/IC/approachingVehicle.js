import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initDropDownList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/common/dictionary`
    }
    return $axios(service, data)
  },
  _initArrivalDropDownList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/arrivalVehicle/dictionary`
    }
    return $axios(service, data)
  },
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/arrivalVehicle/list`
    }
    return $axios(service, data)
  },
  _openHisPiTask (data) {
    // 获取详情页数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/arrivalVehicle/vehicleDetail`
    }
    return $axios(service, data)
  },
  // 导出Excel
  _exportExcel (data) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/exportFile/arrivalVehicle/export`
    }
    return $axios(service, data, 'arraybuffer')
  }
}
