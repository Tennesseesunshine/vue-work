import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initDropDownList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/common/dictionary`
    }
    return $axios(service, data)
  },
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/measureVehicle/list`
    }
    return $axios(service, data)
  },
  _getDeviceStatus (data) {
    // 初始化设备状态
    const service = {
      method: 'get',
      url: `${URLPREFIX}/deviceStatus/listBySiteAndDevice`
    }
    return $axios(service, data)
  },
  _modifyLaneStatus (data) {
    // 修改通道状态
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/measureVehicle/modifyLaneStatus`
    }
    return $axios(service, data)
  },
  _endMeasure (data) {
    // 结束列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/measureVehicle/taskAbandoned`
    }
    return $axios(service, data)
  },
  // 导出Excel
  _exportExcel (data) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/exportFile/measureVehicle/export`
    }
    return $axios(service, data, 'arraybuffer')
  }
}
