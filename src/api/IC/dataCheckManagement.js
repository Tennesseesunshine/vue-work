import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initDropDownList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/measureValidation/initData`
    }
    return $axios(service, data)
  },
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/measureValidation/list`
    }
    return $axios(service, data)
  },
  _initAdd (data) {
    // 添加表单
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/measureValidation/add`
    }
    return $axios(service, data)
  },
  _initModify (data) {
    // 修改表单
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/measureValidation/update`
    }
    return $axios(service, data)
  },
  _initDelete (data) {
    // 删除表格数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/measureValidation/delete`
    }
    return $axios(service, data)
  },
  _initOnly (data) {
    // 获取唯一数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/measureValidation/getById`
    }
    return $axios(service, data)
  },
  _measureValidationExoprt (data) {
    // 数据校验管理导出
    const service = {
      method: 'post',
      url: `${URLPREFIX}/exportFile/measureValidation/export`
    }
    return $axios(service, data, 'arraybuffer')
  }
}
