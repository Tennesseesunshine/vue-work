import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initDropDownList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/common/dictionary`
    }
    return $axios(service, data)
  },
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/businessTiming/list`
    }
    return $axios(service, data)
  },
  _businessDetail (data) {
    // 获取详情页数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/businessTiming/businessDetail`
    }
    return $axios(service, data)
  }
}
