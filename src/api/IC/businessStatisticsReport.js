import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initList (data) {
    // 进出境数量统计
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/statistics/directionCount`
    }
    return $axios(service, data)
  },
  _initListVheicle (data) {
    // 进出境车辆类型统计
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/statistics/vehicleTypeCount`
    }
    return $axios(service, data)
  },
  _initListBusiness (data) {
    // 进出境业务类型统计
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/statistics/businessTypeCount`
    }
    return $axios(service, data)
  }
}
