import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/whiteTrafficRecord/list`
    }
    return $axios(service, data)
  },
  _initDropDownList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/whiteTrafficRecord/site`
    }
    return $axios(service, data)
  }
}
