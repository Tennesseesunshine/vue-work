import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/white/list`
    }
    return $axios(service, data)
  },
  _initAdd (data) {
    // 添加表单
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/white/add`
    }
    return $axios(service, data)
  },
  _initModify (data) {
    // 修改表单
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/white/update`
    }
    return $axios(service, data)
  },
  _initDelete (data) {
    // 删除表格数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/white/delete`
    }
    return $axios(service, data)
  },
  _initOnly (data) {
    // 获取唯一数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/white/getById`
    }
    return $axios(service, data)
  },
  // 导出Excel
  _exportExcel (data) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/exportFile/whiteList/export`
    }
    return $axios(service, data, 'arraybuffer')
  }
}
