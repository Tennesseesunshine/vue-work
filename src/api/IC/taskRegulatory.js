import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _getList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/regulatoryTask/list`
    }
    return $axios(service, data)
  },
  _endProcess (data) {
    // 结束流程
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/inCheckVehicle/endProcess`
    }
    return $axios(service, data)
  },
  _endMeasure (data) {
    // 结束测量
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/regulatoryTask/skipMeasure`
    }
    return $axios(service, data)
  }
}
