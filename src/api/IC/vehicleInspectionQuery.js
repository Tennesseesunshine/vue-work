import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initDropDownList (data) {
    // 获取下拉列表
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/common/dictionary`
    }
    return $axios(service, data)
  },
  _initList (data) {
    // 获取表格列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/ic/inCheckVehicle/list`
    }
    return $axios(service, data)
  },
  _openHisPiTask (data) {
    // 获取详情页数据
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/inCheckVehicle/vehicleDetail`
    }
    return $axios(service, data)
  },
  _unlockVehicle (data) {
    // 解锁在检车辆
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/inCheckVehicle/unlockVehicle`
    }
    return $axios(service, data)
  },
  _measureRecords (data) {
    // 测量记录接口
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/inCheckVehicle/measureRecords`
    }
    return $axios(service, data)
  },
  _getIsExists (data) {
    const service = {
      method: 'get',
      url: `${window.config.Vitracking.commonUrl}/supply/exists-serialnumber`
    }
    return $axios(service, data)
  },
  _endProcess (data) {
    // 结束流程
    const service = {
      method: 'get',
      url: `${URLPREFIX}/ic/inCheckVehicle/endProcess`
    }
    return $axios(service, data)
  },
  // 导出Excel
  _exportExcel (data) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/exportFile/inCheckVehicle/export`
    }
    return $axios(service, data, 'arraybuffer')
  }
}
