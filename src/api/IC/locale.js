import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName
export default {
  accessLog (data) { // 访问日志
    const service = {
      method: 'post',
      url: `${URLPREFIX}/log/access/getPages`
    }
    return $axios(service, data)
  },
  AduitLog (data) { // 审计日志
    const service = {
      method: 'post',
      url: `${URLPREFIX}/log/auditlog/getPages`
    }
    return $axios(service, data)
  },
  auditDetail (data) { // 审计日志详情
    const service = {
      method: 'post',
      url: `${URLPREFIX}/log/auditlog/detail/getPages`
    }
    return $axios(service, data)
  },
  getOperateKeys (data) { // 审计日志操作对象 国际化key列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/log/auditlog/key/getOperateObjectKeys`
    }
    return $axios(service, data)
  }
}
