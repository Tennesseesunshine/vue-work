import $axios from '../../components/common/axios'
// import $trackAxios from '../../components/trackReplay/api/axios.js'
const VIBORDER = window.config.projectName
export default {
  _initBusinessTypeList (data) {
    const servic = {
      // 初始化业务类型下拉列表
      method: 'get',
      url: `${VIBORDER}/dc/initBusinessTypeList`
    }
    return $axios(servic, data)
  },
  _list (data) {
    // 获取待比对车辆列表
    const service = {
      method: 'post',
      url: `${VIBORDER}/dc/list`
    }
    return $axios(service, data)
  },
  _listHis (data) {
    // 获取历史比对车辆列表
    const service = {
      method: 'post',
      url: `${VIBORDER}/dc/listAll`
    }
    return $axios(service, data)
  },
  _isAllowOpen (data) {
    // 列表点击比对时调用
    const service = {
      method: 'post',
      url: `${VIBORDER}/dc/isAllowOpen`
    }
    return $axios(service, data)
  },
  _openCompare (data) {
    // 打开任务进行比对
    const service = {
      method: 'post',
      url: `${VIBORDER}/dc/openCompare`
    }
    return $axios(service, data)
  },
  _openCompareHis (data) {
    // 打开历史比对任务查看详情
    // 解绑数据
    const service = {
      method: 'post',
      url: `${VIBORDER}/dc/openCompareAll`
    }
    return $axios(service, data)
  },
  _submitConclusion (data) {
    // 提交比对结论
    const service = {
      method: 'post',
      url: `${VIBORDER}/dc/submitConclusion`
    }
    return $axios(service, data)
  },
  _cancelCompare (data) {
    // 取消比对
    const service = {
      method: 'get',
      url: `${VIBORDER}/dc/cancelCompare`
    }
    return $axios(service, data)
  },
  _getIsExists (data) {
    const service = {
      method: 'get',
      url: `${window.config.Vitracking.commonUrl}/supply/exists-serialnumber`
    }
    return $axios(service, data)
  },
  _canSubmit (data) {
    const service = {
      method: 'post',
      url: `${VIBORDER}/dc/canSubmit`
    }
    return $axios(service, data)
  }
  // _obtainDCTask (data) {
  //   const service = {
  //     method: 'get',
  //     url: `${VIBORDER}/dc/comparisonStation/obtainDCTask`
  //   }
  //   return $axios(service, data)
  // }
}
