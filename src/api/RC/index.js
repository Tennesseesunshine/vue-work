import $axios from '../../components/common/axios'
const VIBORDER = window.config.projectName
export default {
  _confirmRelease (data) { // 放行确认(ascode自动确认)
    const service = {
      method: 'post',
      url: `${VIBORDER}/releaseConfirm/confirmRelease`
    }
    return $axios(service, data)
  },
  _listReleaseConfirm (data) { // 主页查询放行确认列表
    const service = {
      method: 'post',
      url: `${VIBORDER}/releaseConfirm/listReleaseConfirm`
    }
    return $axios(service, data)
  },
  _listReleaseConfirmHis (data) { // 侧拉框历史在检放行确认任务查询
    const service = {
      method: 'post',
      url: `${VIBORDER}/releaseConfirm/listAll`
    }
    return $axios(service, data)
  },
  _isPrint (data) { // 侧拉框历史在检放行确认任务查询
    const service = {
      method: 'post',
      url: `${VIBORDER}/releaseConfirm/printInfo`
    }
    return $axios(service, data)
  },
  _getPrintData (data) {
    // 获取打印拼条数据
    const service = {
      method: 'post',
      url: `${VIBORDER}/btp/getBtpInfo`
    }
    return $axios(service, data)
  }
}
