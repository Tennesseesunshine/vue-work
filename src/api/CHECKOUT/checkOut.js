import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initData (data) {
    // 获取初始页面的数据
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkOut/initData`
    }
    return $axios(service, data)
  },
  _initDataDetails (data) {
    // 获取初始弹窗详情的数据
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkOut/checkOutDetail`
    }
    return $axios(service, data)
  },
  _getByLpn (data) {
    // 获取lpn车牌号
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkOut/getByLpn`
    }
    return $axios(service, data)
  },
  _getBySocket (data) {
    // 获取lpn车牌号通过socket
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkOut/getBySocket`
    }
    return $axios(service, data)
  },
  _printInfo (data) {
    // 车辆打印信息
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkOut/printInfo`
    }
    return $axios(service, data)
  },
  _manualRelease (data) {
    // 手动放行
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkOut/manualRelease`
    }
    return $axios(service, data)
  },
  _initTableData (data) {
    // 获取初始表格的数据
    const service = {
      method: `post`,
      url: `${URLPREFIX}/checkOut/list`
    }
    return $axios(service, data)
  },
  _getByRfid (data) {
    // 获取rfid
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkOut/getByRfid`
    }
    return $axios(service, data)
  },
  _manualUpBarrier (data) {
    // 抬杆命令
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkOut/manualUpBarrier`
    }
    return $axios(service, data)
  }
}
