import $axios from '../../components/common/axios'
const VIBORDER = window.config.projectName
export default {
  _initBusinessTypeList (data) {
    // 初始化页面
    const service = {
      method: 'get',
      url: `${VIBORDER}/pi/initBusinessTypeList`
    }
    return $axios(service, data)
  },
  _listWaitPiVhicle (data) {
    // 查询手检任务列表
    const service = {
      method: 'post',
      url: `${VIBORDER}/pi/listWaitPiVhicle`
    }
    return $axios(service, data)
  },
  _listHisPiVhicle (data) {
    // 查询历史手检车辆
    const service = {
      method: 'post',
      url: `${VIBORDER}/pi/listAll`
    }
    return $axios(service, data)
  },
  _openPiTask (data) {
    // 打开手检任务
    const service = {
      method: 'get',
      url: `${VIBORDER}/pi/openPiTask`
    }
    return $axios(service, data)
  },
  _openHisPiTask (data) {
    // 打开手检任务
    const service = {
      method: 'get',
      url: `${VIBORDER}/pi/openAllPiTask`
    }
    return $axios(service, data)
  },
  _submitConclusion (data) {
    // 提交手检结论
    const service = {
      method: 'post',
      url: `${VIBORDER}/pi/submitConclusion`
    }
    return $axios(service, data)
  },
  _unLockPiTask (data) {
    // 解锁手检任务
    const service = {
      method: 'post',
      url: `${VIBORDER}/pi/unLockPiTask`
    }
    return $axios(service, data)
  },
  _deletePiFiles (data) {
    // 删除上传文件的接口
    const service = {
      method: 'post',
      url: `${VIBORDER}/pi/deletePiFiles`
    }
    return $axios(service, data)
  },
  _listPiFiles (data) {
    // 点击上传文件按钮时调用的，初始化已上传文件的接口
    const service = {
      method: 'post',
      url: `${VIBORDER}/pi/listPiFiles`
    }
    return $axios(service, data)
  },
  _addedTask (data) {
    // 增加任务
    const service = {
      method: 'get',
      url: `${VIBORDER}/pi/addedTask`
    }
    return $axios(service, data)
  },
  _canSubmit (data) {
    const service = {
      method: 'post',
      url: `${VIBORDER}/pi/canSubmit`
    }
    return $axios(service, data)
  }
}
