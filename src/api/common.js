import $axios from '../components/common/axios.js'
const URLPREFIX = window.config.projectName

export default {
  login (data) {
    const service = {
      method: 'post',
      url: '/user/login'
    }
    return $axios(service, data)
  },
  changePassword (data) {
    const service = {
      method: 'post',
      url: '/user/changePassword'
    }
    return $axios(service, data)
  },
  initPwdRule (data) {
    const service = {
      method: 'post',
      url: '/user/initPwdRule'
    }
    return $axios(service, data)
  },
  logout (data) {
    const service = {
      method: 'post',
      url: '/user/logout'
    }
    return $axios(service, data)
  },
  getPrmList (data) {
    const service = {
      method: 'post',
      url: '/prm/getPrmCodeList'
    }
    return $axios(service, data)
  },
  chooseLogin (data) {
    const service = {
      method: 'get',
      url: '/user/isLogon'
    }
    return $axios(service, data)
  },
  _getDropDownList (data) {
    // 登录页获取下拉区域道号
    const service = {
      method: `get`,
      url: `${URLPREFIX}/whiteList/initData/initSiteLaneNumber`
    }
    return $axios(service, data)
  },
  _getKaptcha (data) {
    const service = {
      method: 'get',
      url: '/kaptcha/captcha?t=' + new Date().getTime()
    }
    return $axios(service, data)
  },
  _getLang (data) {
    const service = {
      method: 'post',
      url: '/i18n/vueJsonByLang'
    }
    return $axios(service, data)
  },
  _getDeviceStatus (data, uri) {
    // 初始化设备状态
    const service = {
      method: 'get',
      url: `${URLPREFIX}/deviceStatus/${uri}`
    }
    return $axios(service, data)
  }
}
