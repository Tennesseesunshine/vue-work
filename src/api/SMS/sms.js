import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initDeviceStatus (data) { // 初始化设备状态
    const service = {
      method: 'get',
      url: `${URLPREFIX}/deviceStatus/list`
    }
    return $axios(service, data)
  },
  _vehicleMeasure (data) { // 测量车辆列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/im/vehicleMeasure/list`
    }
    return $axios(service, data)
  },
  _measureValidation (data) { // 获取校验数据项
    const service = {
      method: 'get',
      url: `${URLPREFIX}/im/measureValidation/list`
    }
    return $axios(service, data)
  },
  _switchDeviceStatus (data) { // 切换红绿灯和罗抬杆
    const service = {
      method: 'post',
      url: `${URLPREFIX}/im/deviceCommand/switchDeviceStatus`
    }
    return $axios(service, data)
  },
  _switchDeviceMode (data) { // 切换设备模式
    const service = {
      method: 'post',
      url: `${URLPREFIX}/im/deviceCommand/switchDeviceMode`
    }
    return $axios(service, data)
  },
  _finish (data) { // 结束测量
    const service = {
      method: 'post',
      url: `${URLPREFIX}/im/vehicleMeasure/endMeasure`
    }
    return $axios(service, data)
  },
  _lmiStatusRequest (data) { // 长宽高激光头状态
    const service = {
      method: 'post',
      url: `${URLPREFIX}/im/deviceCommand/lmiStatusRequest`
    }
    return $axios(service, data)
  },
  _clearChannelOccupy (data) { // 清除通道
    const service = {
      method: 'post',
      url: `${URLPREFIX}/im/innerEvent/clearChannelOccupy`
    }
    return $axios(service, data)
  },
  _getDropDownlist (data) { // 下拉列表
    const service = {
      method: 'post',
      url: `${URLPREFIX}/im/vehicleMeasure/initData`
    }
    return $axios(service, data)
  },
  _smsHis (data) { // 历史
    const service = {
      method: 'post',
      url: `${URLPREFIX}/im/vehicleMeasure/listAll`
    }
    return $axios(service, data)
  },
  _exportExcel (data) {
    const service = {
      method: 'post',
      url: `${URLPREFIX}/exportFile/measure/export`
    }
    return $axios(service, data, 'arraybuffer')
  }
}
