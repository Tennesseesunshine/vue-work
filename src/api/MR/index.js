import $axios from '../../components/common/axios'
const VIBORDER = window.config.projectName
export default {
  _findByAsycudaCode (data) { // 根据asycuda查询待人工放行数据
    const service = {
      method: 'post',
      url: `${VIBORDER}/manualReleaseConifrm/findByAsycudaCode`
    }
    return $axios(service, data)
  },
  _listWaitManualReleaseConfirm (data) { // 人工放行数据查询
    const service = {
      method: 'post',
      url: `${VIBORDER}/manualReleaseConifrm/listWaitManualReleaseConfirm`
    }
    return $axios(service, data)
  },
  _findDetail (data) { // 查看人工放行详情
    const service = {
      method: 'get',
      url: `${VIBORDER}/manualReleaseConifrm/findDetail`
    }
    return $axios(service, data)
  },
  _manualConfirm (data) { // 人工放行确认
    const service = {
      method: 'post',
      url: `${VIBORDER}/manualReleaseConifrm/manualConfirm`
    }
    return $axios(service, data)
  },
  _uploadDriverPhoto (data) { // 上传司机照片
    const service = {
      method: 'post',
      url: `${VIBORDER}/manualReleaseConifrm/uploadDriverPhoto`
    }
    return $axios(service, data)
  },
  _interceptVideo (data) { // 视频截取
    const service = {
      method: 'post',
      url: `${VIBORDER}/manualReleaseConifrm/interceptVideo`
    }
    return $axios(service, data)
  }
}
