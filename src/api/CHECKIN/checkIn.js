import $axios from '../../components/common/axios'
const URLPREFIX = window.config.projectName

export default {
  _initData (data) {
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkIn/initData`
    }
    return $axios(service, data)
  },
  _initList (data) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/checkIn/listAll`
    }
    return $axios(service, data)
  },
  _getBylpn (data) {
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkIn/getByLpn`
    }
    return $axios(service, data)
  },
  _getByAsycudaCode (data) {
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkIn/getByAsycudaCode`
    }
    return $axios(service, data)
  },
  _submit (data) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/checkIn/register`,
      arrayFormat: 'repeat'
    }
    return $axios(service, data)
  },
  _abandoned (data) {
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkIn/taskAbandoned`
    }
    return $axios(service, data)
  },
  _getDetails (data) {
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkIn/findDetail`
    }
    return $axios(service, data)
  },
  _initDetailList (data) {
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkIn/initDetailList`
    }
    return $axios(service, data)
  },
  _uploadDriverPhoto (data) {
    const service = {
      method: `post`,
      url: `${URLPREFIX}/checkIn/uploadDriverPhoto`
    }
    return $axios(service, data)
  },
  _isRelease (data) {
    const service = {
      method: `get`,
      url: `${URLPREFIX}/checkIn/whiteListVehicleRelease`
    }
    return $axios(service, data)
  }
}
