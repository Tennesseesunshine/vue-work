import Mock from 'mockjs'

const newTable = []
let Random = Mock.Random
for (let i = 0; i < 20; i++) {
  newTable.push(
    Mock.mock({
      id: Random.integer(60, 100),
      Business: [],
      checkInId: Random.guid(),
      CheckIn: Random.datetime(),
      Container: Random.character() + Random.natural(10000),
      Predeclaration: Random.character() + Random.natural(10000),
      Vehicle: []
    })
  )
}
export { newTable }
