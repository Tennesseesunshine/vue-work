import { initTable, imgArr } from './CS/cis'
import { newTable } from './IC/ic'
const uuidv1 = require('uuid/v1')
let axios = require('axios')
let MockAdapter = require('axios-mock-adapter')

console.log('initTable', initTable)
console.log('newTable', newTable)

export default {
  init () {
    function filter (source, keyObj) {
      let keys = Object.keys(keyObj)
      return source.filter(v => {
        return keys.every(key => v[key] === keyObj[key])
      })
    }
    let mock = new MockAdapter(axios)
    let resData = []
    let paging = []
    let dataLen = ''
    mock.onGet('/cs/cis/initData').reply(config => {
      return new Promise((resolve, reject) => {
        resolve([
          200,
          {
            result: {
              isTrans: true,
              isExistRFIDCard: '1',
              data: initTable[Math.floor(Math.random() * 20)]
            },
            code: 1001,
            msg: '请求成功',
            flag: true
          }
        ])
      })
    })
    mock.onGet('/cs/cis/initTable').reply(config => {
      // console.log('config', config)
      return new Promise((resolve, reject) => {
        let parmLen = Object.keys(config.params).length
        if (
          // 这里有初始化和重置之后直接查询
          parmLen === 2 &&
          (config.params.size &&
            (config.params.page === 0 || config.params.page))
        ) {
          resData = filter(initTable, {})
          if (resData.length > config.params.size) {
            if (config.params.page === 0) {
              paging = resData.slice(0, config.params.size)
            } else {
              paging = resData.slice(config.params.size, resData.length)
            }
          } else {
          }
          dataLen = resData.length
        } else {
          // 这里是带有分页和查询字段的
          let pData = {
            size: config.params.size,
            page: config.params.page
          }
          if (config.params.page || config.params.size) {
            delete config.params.size
            delete config.params.page
            resData = filter(initTable, config.params)
            if (resData.length >= pData.size) {
              if (config.params.page === 0) {
                paging = resData.slice(0, pData.size)
              } else {
                paging = resData.slice(pData.size, resData.length)
              }
            } else {
              paging = filter(initTable, config.params)
            }
          } else {
            paging = filter(initTable, config.params)
          }
          dataLen = paging.length
        }
        resolve([
          200,
          {
            result: {
              content: paging,
              size: 10,
              number: 0,
              totalElements: dataLen
            },
            code: 1001,
            msg: '请求成功',
            flag: true
          }
        ])
      })
    })
    mock.onPost('/cs/cis/initForm').reply(config => {
      return new Promise((resolve, reject) => {
        let parm = JSON.parse(config.data)
        for (let key in parm) {
          resData = initTable.find(e => {
            return e[key] === parm[key]
          })
        }
        resolve([
          200,
          {
            result: {
              isTrans: false,
              data: resData
            },
            code: 1001,
            msg: '请求成功',
            flag: true
          }
        ])
      })
    })
    mock.onPost('/cs/cis/submitInfo').reply(config => {
      return new Promise((resolve, reject) => {
        let parm = JSON.parse(config.data)
        Object.assign(
          parm,
          { checkInId: uuidv1() },
          { CheckIn: new Date().toLocaleString() }
        )
        initTable.unshift(parm)
        resolve([
          200,
          {
            result: initTable,
            code: 1001,
            msg: '请求成功',
            flag: true
          }
        ])
      })
    })
    mock.onPost('/cs/cis/addImg').reply(config => {
      return new Promise((resolve, reject) => {
        resolve([
          200,
          {
            result: imgArr,
            code: 1001,
            msg: '请求成功',
            flag: true
          }
        ])
      })
    })
    mock.onPost('/sms/switchTrafficLight').reply(config => {
      return new Promise((resolve, reject) => {
        resolve([
          200,
          {
            result: [],
            code: 1001,
            msg: '请求成功',
            flag: true
          }
        ])
      })
    })
    mock.onPost('/sms/switchBarrier').reply(config => {
      return new Promise((resolve, reject) => {
        resolve([
          200,
          {
            result: [],
            code: 1001,
            msg: '请求成功',
            flag: true
          }
        ])
      })
    })
    mock.onGet('/ic/ic/newTable').reply(config => {
      // console.log('config', config)
      return new Promise((resolve, reject) => {
        let parmLen = Object.keys(config.params).length
        if (
          // 这里有初始化和重置之后直接查询
          parmLen === 2 &&
          (config.params.size &&
            (config.params.page === 0 || config.params.page))
        ) {
          resData = filter(newTable, {})
          if (resData.length > config.params.size) {
            if (config.params.page === 0) {
              paging = resData.slice(0, config.params.size)
            } else {
              paging = resData.slice(config.params.size, resData.length)
            }
          } else {
          }
          dataLen = resData.length
        } else {
          // 这里是带有分页和查询字段的
          let pData = {
            size: config.params.size,
            page: config.params.page
          }
          if (config.params.page || config.params.size) {
            delete config.params.size
            delete config.params.page
            resData = filter(initTable, config.params)
            if (resData.length >= pData.size) {
              if (config.params.page === 0) {
                paging = resData.slice(0, pData.size)
              } else {
                paging = resData.slice(pData.size, resData.length)
              }
            } else {
              paging = filter(initTable, config.params)
            }
          } else {
            paging = filter(initTable, config.params)
          }
          dataLen = paging.length
        }
        resolve([
          200,
          {
            result: {
              content: paging,
              size: 10,
              number: 0,
              totalElements: dataLen
            },
            code: 1001,
            msg: '请求成功',
            flag: true
          }
        ])
      })
    })
  }
}
