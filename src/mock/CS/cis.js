import Mock from 'mockjs'

const initTable = []
let Random = Mock.Random
let Business = [
  'Import',
  'Transit to Tariff Union',
  'Transit to Non-tariff Union',
  'Transit to Domestic'
]
let bool = ['0', '1']
let Vehicle = [
  'Vehicle',
  'Unloaded Truck',
  'Bus',
  'Private Car',
  'Commodity Car',
  'Official Car'
]
let Destination = ['Alma-Ata', 'Kolzhat', ' ']
let Risk = ['0', '1', '2']
let Document1 = [
  'Contract',
  'Freight Note',
  'Invoice',
  'Declaration',
  'Passport',
  'Other'
]
for (let i = 0; i < 20; i++) {
  initTable.push(
    Mock.mock({
      id: Random.integer(60, 100),
      checkInId: Random.guid(),
      CheckIn: Random.datetime(),
      'originPortName|1': Destination,
      licensePlateNumber: Random.string('lower', 15),
      arrivalTime: Random.datetime(),
      rfid: Random.character() + Random.natural(10000),
      asycudaCode: Random.string('lower', 15),
      declarationNumber: Random.character() + Random.natural(10000),
      'logistics|1': bool,
      'transitPortName|1': Destination,
      'clearanceTypes|1': Business,
      'vehicleTypes|1': Vehicle,
      'entryTunnel|1': bool,
      'xrayScan|1': bool,
      'riskLevels|1': Risk,
      'checkInScannedType|1': Document1,
      'Trailer|1': bool,
      remark: Random.paragraph(1, 3),
      driverPhotoPath: '',
      imgUrl: []
    })
  )
}

let imgArr = [
  'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1528351544169&di=aaa00c75ca32ca8c71aea82d14f68a27&imgtype=0&src=http%3A%2F%2Fimg.zcool.cn%2Fcommunity%2F01dc6a5545831a0000019ae92a8f40.jpg%402o.jpg',
  'http://pic1.16pic.com/00/37/28/16pic_3728619_b.jpg',
  'http://pic6.nipic.com/20100415/4321211_002313964413_2.jpg'
]

export { initTable, imgArr }
