import Vue from 'vue'
const drag = Vue.directive('drag', {
  inserted: function (el) {
    var el1 = document.getElementById('drag-box-parent')
    var flag, bindingTop, dataUrl
    el.onmousedown = function (ev) {
      if (dataUrl !== el.getAttribute('data-url')) {
        dataUrl = el.getAttribute('data-url')
        flag = true
      }
      if (flag) {
        bindingTop = el.offsetTop
        flag = false
        console.log(bindingTop)
      }
      var disX = ev.clientX - el.offsetLeft
      var disY = ev.clientY - el.offsetTop
      document.onmousemove = function (ev) {
        var scale, rotate
        if (el.style.transform === '') {
          scale = 1
          rotate = 180
        } else {
          scale = el.style.transform.split(' ')[2].split(')')[0]
          rotate = Math.abs(el.style.transform.split(' ')[0].split('(')[1].split('d')[0]) + 180
        }
        var l = ev.clientX - disX
        var t = ev.clientY - disY
        if (rotate / 90 % 2 === 0) {
          if ((el1.offsetHeight >= el.offsetHeight * scale) && (el1.offsetWidth >= el.offsetWidth * scale)) {
            return false
          }
          if (el1.offsetWidth < el.offsetWidth * scale) {
            if (Math.abs(l * 2) + el1.offsetWidth < el.offsetWidth * scale) {
              el.style.left = l + 'px'
            }
          }
          if (el1.offsetHeight < el.offsetHeight * scale) {
            const perHeight = el.offsetHeight * scale - el1.offsetHeight
            if (Math.abs(t - bindingTop) < perHeight / 2) {
              el.style.top = t + 'px'
            }
          }
        } else {
          if ((el1.offsetHeight >= el.offsetWidth * scale) && (el1.offsetWidth >= el.offsetHeight * scale)) {
            return false
          }
          if (el1.offsetWidth < el.offsetHeight * scale) {
            if (Math.abs(l * 2) + el1.offsetWidth < el.offsetHeight * scale) {
              el.style.left = l + 'px'
            }
          }
          if (el1.offsetHeight < el.offsetWidth * scale) {
            const perHeight = el.offsetWidth * scale - el1.offsetHeight
            if (Math.abs(t - bindingTop) < perHeight / 2) {
              el.style.top = t + 'px'
            }
          }
        }
      }
      document.onmouseup = function () {
        document.onmousemove = null
        document.onmouseup = null
      }
      return false // 必须要有 防止拖动产生异常 拖动结束退出函数
    }
  }
})

const focus = Vue.directive('focus', function (el, binding) {
  console.log(el)
})
Vue.prototype.$formatDate = function (value, format) {
  if (value === null || value === '' || value === undefined) {
    return
  }
  var paddNum = function (num) {
    num += ''
    return num.replace(/^(\d)$/, '0$1')
  }
  var date = new Date(parseInt(value))
  var cfg = {
    yyyy: date.getFullYear(),
    yy: date
      .getFullYear()
      .toString()
      .substring(2),
    M: date.getMonth() + 1,
    MM: paddNum(date.getMonth() + 1),
    d: date.getDate(),
    dd: paddNum(date.getDate()),
    HH: paddNum(date.getHours()),
    mm: paddNum(date.getMinutes()),
    ss: paddNum(date.getSeconds())
  }
  return format.replace(/([a-z])(\1)*/gi, function (m) {
    return cfg[m]
  })
}
export default { drag, focus }
