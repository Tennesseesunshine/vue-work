// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import 'babel-polyfill'
import vueBar from 'vuebar'
import get from 'lodash/get'
import router from './router'
import echarts from 'echarts'
import videojs from 'video.js'
import VueI18n from 'vue-i18n'
import store from './vuex/store'
import ElementUI from 'element-ui'
import 'video.js/dist/video-js.css'
import handleData from './api/common'
import Chinese from './locale/Chinese'
import English from './locale/English'
import Russian from './locale/Russian'
import '@/assets/iconfont/iconfont.css'
import VideoEN from './locale/video-en'
import VideoRU from './locale/video-ru'
import VideoPlayer from 'vue-video-player'
import 'element-ui/lib/theme-chalk/index.css'
import 'vue-video-player/src/custom-theme.css'
import directive from './directives/directive'
import filter from './filter/filter'
import rules from '../src/components/common/rules'
import {
  formatDateTime,
  splitArr,
  isExitUrl,
  filterNullFields,
  customValidateTimeRange,
  resetTime,
  createBlobUrl
} from './lib/index'
import { startStopTime } from './components/IC/index.js'
import { getConclusion, otherHoverData } from '../src/components/common/unifiedLogic'
import panelTitle from './components/common/title'
import tableForm from './components/common/tableForm'
import ScanDoc from './components/common/scanDoc.vue'
import singleImgView from './components/common/singleImgView'
import Viewer from './components/common/viewer'
import ComForm from './components/common/form'
import ComTable from './components/common/table'
import Dialog from './components/common/dialog'
import otherSystem from './components/common/otherSystem'
import breadCrumb from './components/common/breadCrumb'

Vue.component('panelTitle', panelTitle)
Vue.component('tableForm', tableForm)
Vue.component('Dialog', Dialog)
Vue.component('otherSystem', otherSystem)
Vue.component('breadCrumb', breadCrumb)
Vue.component('ScanDoc', ScanDoc)
Vue.component('singleImgView', singleImgView)
Vue.component('Viewer', Viewer)
Vue.component('ComForm', ComForm)
Vue.component('ComTable', ComTable)

const fileServerUrl = `${window.config.baseUrl}${window.config.fileServerUrl}`
videojs.addLanguage('en-US', VideoEN)
videojs.addLanguage('ru-RU', VideoRU)
Vue.use(vueBar)
Vue.use(VueI18n)
Vue.use(ElementUI)
Vue.use(VideoPlayer)
Vue.config.productionTip = false
// _.get方法获取对象深层参数
Vue.prototype.$_get = get
// 是否显示左侧菜单栏
Vue.prototype.isShowMenu = true
Vue.prototype.$rules = rules
Vue.prototype.$echarts = echarts
Vue.prototype.$splitArr = splitArr
Vue.prototype.$isExitUrl = isExitUrl
Vue.prototype.$getConclusion = getConclusion
Vue.prototype.$otherHoverData = otherHoverData
Vue.prototype.$formatDateTime = formatDateTime
Vue.prototype.$filterNullFields = filterNullFields
Vue.prototype.$startStopTime = startStopTime
Vue.prototype.$customValidateTimeRange = customValidateTimeRange
Vue.prototype.$resetTime = resetTime
Vue.prototype.$createBlobUrl = createBlobUrl
// 挂载提示位置
Vue.prototype.$notifyOffset = window.config.notifyOffset
// 文件名
Vue.prototype.$lprFolder = `${fileServerUrl}${window.config.fileFolder.lprFolder}`
Vue.prototype.$photoFolder = `${fileServerUrl}${window.config.fileFolder.photoFolder}`
Vue.prototype.$manifestFolder = `${fileServerUrl}${window.config.fileFolder.manifestFolder}`
// socket地址
Vue.prototype.$viborderStompWebsocket = `${window.config.baseUrl}${window.config.projectName}${window.config.viborderStompWebsocket}`

const i18n = new VueI18n({
  locale: localStorage.lang || window.config.initLang,
  messages: {
    'zh-CN': Chinese,
    'en-US': English,
    'ru-RU': Russian
  }
})

function loadLocaleMessage (locale, cb) {
  return handleData
    ._getLang({ path: 'ViBorder', lang: locale })
    .then(res => {
      if (res.data.flag) {
        cb(res.data.result)
      }
    })
    .catch(err => {
      console.log('err', err)
    })
}
loadLocaleMessage('en-US', message => {
  i18n.setLocaleMessage('en-US', message)
  loadLocaleMessage('ru-RU', message => {
    i18n.setLocaleMessage('ru-RU', message)
  })
})
// loadLocaleMessage('zh-CN', message => {
//   i18n.setLocaleMessage('zh-CN', message)
// })

/* eslint-disable no-new */
const vm = new Vue({
  el: '#app',
  router,
  i18n,
  store,
  components: { App },
  template: '<App/>'
})
export default vm
