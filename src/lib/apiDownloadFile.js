/**
 * 下载图片
 * @param {*} url blob创建的文件流
 * @param {*} filename 文件名称
 */
const saveFile = (url, filename, exportType) => {
  let link = document.createElementNS('http://www.w3.org/1999/xhtml', 'a') // 创建一个带有命名空间的a标签
  link.href = url
  if (!exportType || exportType !== 'print') {
    link.download = filename
  }
  if (exportType === 'print') {
    link.target = '_blank'
  }
  link.click()
}
/**
 *
 * @param {*} data 数据流
 * @param {*} type pdf/xlsx
 * @param {*} title 标题
 * @param {*} exportType 下载还是打印
 */
const createBlobUrl = (data, type, title, exportType) => {
  console.log(`%c${type}`, 'color:#3cc51f;')
  let blob = new Blob([data], {
    type
  })
  // 创建url
  let url = URL.createObjectURL(blob)
  // 利用blob创建的url去下载
  saveFile(url, title, exportType)
}

export default createBlobUrl

export function initDownloadOptions ({
  exportType,
  pageType,
  titleKey,
  size,
  tableGroup}) {
  let parm = {
    exportType,
    pageType,
    titleKey,
    lang: localStorage.getItem('lang'),
    size
  }
  let o = {}
  let a = tableGroup.slice()
  if (a.find(e => {
    return e.model === 'operation'
  })) {
    a.pop()
  }
  if (a.find(e => {
    return e.type === 'selection'
  })) {
    a.shift()
  }
  a.map(e => {
    return {
      field: e.model.includes('.') ? e.model.split('.')[0] : e.model,
      title: e.label,
      colWidth: e.width || 100
    }
  }).forEach((e, i) => {
    o = Object.assign(o, {
      [`header[${i}].field`]: e.field,
      [`header[${i}].title`]: e.title,
      [`header[${i}].colWidth`]: e.colWidth
    })
  })
  return {
    ...o,
    ...parm
  }
}
