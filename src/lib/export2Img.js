import html2canvas from 'html2canvas'
import { Loading } from 'element-ui'
import vm from '@/main.js'

/**
 * 下载图片
 * @param {*} url blob创建的文件流
 * @param {*} filename 文件名称
 */
const saveFile = (url, filename) => {
  let link = document.createElementNS('http://www.w3.org/1999/xhtml', 'a') // 创建一个带有命名空间的a标签
  link.href = url
  link.download = filename
  link.click()
}

/**
 * 将base64转成blob
 * @param {*} dataStream base64文件流
 */
const dataURIToBlob = dataStream => {
  let arr = dataStream.split(',')
  let mime = arr[0].match(/:(.*?);/)[1]
  let bstr = atob(arr[1])
  let n = bstr.length
  let u8arr = new Uint8Array(n)
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }
  return new Blob([u8arr], { type: mime })
}

/**
 * 导出img图片
 * @param {*} el 被导出的元素
 * @param {*} fileName 传入的文件名称
 */
const export2Img = ({ el, fileName, cb }) => {
  const loading = Loading.service({
    lock: true,
    text: vm.$t('ViBorder.common.loading'),
    spinner: 'el-icon-loading',
    background: 'rgba(0, 0, 0, 0.7)'
  })
  html2canvas(el, { scale: 2, logging: false, useCORS: true }).then(canvas => {
    let type = 'jpg'
    let imgData = canvas.toDataURL(type)
    // 照片格式处理
    let _fixType = type => {
      type = type.toLowerCase().replace(/jpg/i, 'jpeg')
      let r = type.match(/png|jpeg|bmp|gif/)[0]
      return 'image/' + r
    }
    imgData = imgData.replace(_fixType(type), 'image/octet-stream')
    let filename = fileName + '.' + type
    // 将base64转成blob
    let blob = dataURIToBlob(imgData)
    // 创建url
    let url = URL.createObjectURL(blob)
    // 利用blob创建的url去下载
    saveFile(url, filename)
    loading.close()
    if (cb) {
      cb()
    }
  })
}

export default export2Img
