import {
  initViProbeWebSocket,
  // viProbeWsCallback,
  viProbeOperation,
  viProbeJsonUtil,
  reconnectToViProbeFailed
} from './viProbeUtil'
import { Notification } from 'element-ui'
const elementcss = require('element-ui/lib/theme-chalk/index.css')
/*
 * 版权所有 ( c ) 同方威视技术股份有限公司 2018 保留所有权利。
 * 项目：ViCenter-WEB
 * 文件名：viProbeForIns.js
 * 描述：查验子系统 与viProbe交互的页面使用，依赖viProbeUtil.js文件
 */
// 初始化WebSocket连接
// 由于浏览器限制的原因，每个页面可以建立的WebSocket连接有限制，故建议一个页面共用同一个socket连接
initViProbeWebSocket()
// 传输协议，ViProbe与文件服务器之间的传输协议
var transType = 'https'
// 当调用获取嫌疑标记方法后根据返回结果设设置下面两个字段的值
// 获取嫌疑标记信息成功与否，默认为null，
var getMarkBase64Success = null
// 嫌疑标记信息的BASE64编码
var markBase64 = null
// 获取新嫌疑标记信息成功与否，默认为null
var getNewMarksSuccess = null
// 新嫌疑标记信息的BASE64编码
var markNew = null
// 提交图像分析结果相关文件是否成功，根据调用方法后的返回结果，设置此字段值
var uploadImageAnalysisSuccess = null
// 获取用户在ViProbe中变换的最终图像是否成功
var getImageInViProbeBase64Success = null
// ViProbe中变换后的最终图像的BASE64编码，
// 是一个字符串数组，用逗号分隔，以支持双视角图像，如果是单视角图像则数组的大小为1，否则为双视角图像返回的数组大小为2
var imageInViProbeBase64 = null
var reconnectToViProbeFailedNew = reconnectToViProbeFailed
var errorCode = 0
/**
 * 生成打开图像的Command
 * @param name
 * 图像流水号，即任务号
 * @param path
 * 图像路径，到文件夹级别
 * @param imageFiles
 * 图像数组，文件数组，其中sign和md5根据项目可选
 * [{"name":"XXXXX.img","sign":"xxxx","md5":"md5code"},…,{ "name":"XXXX.img","sign":"xxxx","md5":"md5code"}]
 * @param openMode
 * @returns
 */
function generateOpenImageCommandObj (name, path, imageFiles, openMode) {
  var ext = 'img'
  // 传值不匹配时，viprobe自动识别打开的图像类型，此处不能传空值或空串。
  var type = 'auto'
  var trans = transType
  var repname = null
  var closable = false
  // var imageArr = new Array()
  var imageArr = []
  imageArr.push(
    viProbeJsonUtil.generateImageObj(
      name,
      path,
      imageFiles,
      ext,
      type,
      trans,
      repname,
      closable
    )
  )
  var param = {}
  if (openMode) {
    param.mode = openMode
  } else {
    param.mode = 'default'
  }
  param.images = imageArr
  var openCommandObj = viProbeJsonUtil.generateCommandObj('open', param)
  console.debug('generateOpenImageJsonObj: ' + JSON.stringify(openCommandObj))
  return openCommandObj
}
/**
 * 在图像上显示嫌疑标记命令，嫌疑标记不可编辑
 * @param name
 * 图像名称
 * @param markFilePath
 * 嫌疑标记文件的绝对路径
 * @returns
 */
function generateShowHistoryMarkCommandObj (name, markFilePath) {
  var type = transType
  var content = markFilePath
  var show = true
  var color = null
  var editable = false // 不可编辑
  var deleteable = false // 不可删除
  var markObj = viProbeJsonUtil.generateMarkObj(
    type,
    content,
    show,
    color,
    editable,
    deleteable
  )
  var markArr = []
  markArr.push(markObj)
  var param = {}
  param.name = name
  param.marks = markArr
  var showMarkCommandObj = viProbeJsonUtil.generateCommandObj('add_mark', param)
  return showMarkCommandObj
}
/**
 * 生成加载历史操作步骤命令
 * @param name
 * 图像名称
 * @param stepFilePath
 * 操作步骤文件的绝对路径
 * @returns
 */
function generateLoadStepCommandObj (name, stepFilePath) {
  var type = transType
  var content = stepFilePath
  // 不自动播放历史操作步骤，false
  // 20180907由于设置为false时viprobe有bug，此值暂时设计为null，此bug在viprobe的下一版本中修改
  var autoPlay = null
  var stepObj = viProbeJsonUtil.generateStepObj(type, content, autoPlay)
  var param = {}
  param.name = name
  param.step = stepObj
  var loadStepCommandObj = viProbeJsonUtil.generateCommandObj(
    'set_history',
    param
  )
  return loadStepCommandObj
}

/**
 * 生成上传图像分析信息的命令，包含嫌疑标记文件、历史操作步骤、带嫌疑标记的jpg文件
 * @param name
 * 图像名称
 * @param path
 * 图像要上传到的文件夹路径
 * @param isMarkUpload
 * 嫌疑标记是否上传，true/false
 * @param isStepUpload
 * 操作步骤是否上传，true/false
 * @param isMarkJpgUpload
 * 带嫌疑标记的jpg图像是否上传，true/false
 * @returns
 */
function generateUploadAnalysisInfoCommand (
  name,
  path,
  isMarkUpload,
  isStepUpload,
  isMarkJpgUpload
) {
  var trans = transType
  var markColor = null
  var uploadObj = viProbeJsonUtil.generateUploadObj(
    trans,
    name,
    path,
    markColor,
    isMarkUpload,
    isStepUpload,
    isMarkJpgUpload
  )
  var uploadCommandObj = viProbeJsonUtil.generateCommandObj('upload', uploadObj)
  return uploadCommandObj
}
/**
 * 生成获取Viprobe中最终审图结果图像的命令
 * @param name
 * 图像名称
 * @returns
 */
function generateGetImageBase64Command (name) {
  var original = false
  var lineWidth = null
  var viewport = false
  var includeMark = true
  var includeRuler = true
  var getImageBase64Obj = viProbeJsonUtil.generateGetImageBase64Obj(
    name,
    original,
    lineWidth,
    viewport,
    includeMark,
    includeRuler
  )
  var uploadCommandObj = viProbeJsonUtil.generateCommandObj(
    'get_image_as_base64',
    getImageBase64Obj
  )
  return uploadCommandObj
}

/**
 * 打开（一个扫描任务的）扫描图像
 * @returns
 */
function openImageInViProbe (name, path, imageFiles) {
  // 仅支持同时打开 一个扫描图像，且扫描图像与任务详情页面同步打开，同步关闭
  // 图像打开方式，不允许用户通过ViProbe手动关闭，且仅打开图像即可
  var openCommandObj = generateOpenImageCommandObj(name, path, imageFiles)
  viProbeOperation.sendCommand(openCommandObj)
}

/**
 * 打开（一个手检或历史任务的）扫描图像
 * 仅支持同时打开一个扫描图像，且扫描图像与任务详情页面同步打开，同步关闭
 * 图像打开方式，不允许用户通过ViProbe手动关闭
 * 打开图像时，需要同时以只读的方式打开嫌疑标记信息、历史操作步骤等。
 * @returns
 */
function openImageAndAnalysisInfoInViProbe (
  name,
  path,
  imageFiles,
  markFilePath,
  stepFilePath
) {
  // 打开图像
  openImageInViProbe(name, path, imageFiles)
  // 加载嫌疑标记文件
  if (markFilePath) {
    var showMarkCommandObj = generateShowHistoryMarkCommandObj(
      name,
      markFilePath
    )
    viProbeOperation.sendCommand(showMarkCommandObj)
  }
  // 加载操作步骤文件
  if (stepFilePath) {
    var loadStepCommandObj = generateLoadStepCommandObj(name, stepFilePath)
    viProbeOperation.sendCommand(loadStepCommandObj)
  }
}

/**
 * 关闭图像
 * @param name
 * 图像名称
 * @returns
 */
function closeImageInViProbe (name) {
  var param = {
    name: name
  }
  var closeCommandObj = viProbeJsonUtil.generateCommandObj('close', param)
  viProbeOperation.sendCommand(closeCommandObj)
}

/**
 * 关闭ViProbe中的所有图像
 * @returns
 */
function closeAllImageInViProbe () {
  var param = {}
  var closeAllCommandObj = viProbeJsonUtil.generateCommandObj(
    'close_all',
    param
  )
  viProbeOperation.sendCommand(closeAllCommandObj)
}

/**
 * 获取用户在viprobe上画的嫌疑标记信息的BASE64编码
 * @param name
 * 图像名称
 * @returns
 */
function getMarkAsBase64 (name) {
  var param = {
    name: name,
    visible: true
  }
  var getMarkBase64CommandObj = viProbeJsonUtil.generateCommandObj(
    'get_mark_as_base64',
    param
  )
  viProbeOperation.sendCommand(getMarkBase64CommandObj)
}

/**
 * 获取用户在viprobe上画的新嫌疑标记信息的JSON编码
 * @param name 图像名称
 * @returns
 */
function getNewMarks (name) {
  var param = { name: name, visible: true }
  var getNewMarksCommandObj = viProbeJsonUtil.generateCommandObj(
    'get_new_marks',
    param
  )
  viProbeOperation.sendCommand(getNewMarksCommandObj)
}
/**
 * 上传图像分析相关信息
 * @param name
 * 图像名称
 * @returns
 */
function uploadImageAnalysisInfo (
  name,
  path,
  isMarkUpload,
  isStepUpload,
  isMarkJpgUpload
) {
  var uploadAnalysisInfoCommandObj = generateUploadAnalysisInfoCommand(
    name,
    path,
    isMarkUpload,
    isStepUpload,
    isMarkJpgUpload
  )
  viProbeOperation.sendCommand(uploadAnalysisInfoCommandObj)
}
/**
 * 用户在ViProbe中变换的最终图像
 * @param name
 * 图像名称
 * @returns
 */
function getImageInViProbeBase64 (name) {
  var getImageBase64CommandObj = generateGetImageBase64Command(name)
  viProbeOperation.sendCommand(getImageBase64CommandObj)
}
// 根据需要重写viProbeWsCallback对象中的某些方法，覆盖viProbeUtil.js中的方法
// 即各动作操作成功后需要的操作，可以写在相应的回调函数中
/**
 * 重写调用打开图像后的onMessage方法
 */
// viProbeWsCallback.open = function (jsonObj) {
//   console.debug('OnMessage call #' + jsonObj.command + '(rewrite) method')
//   switch (jsonObj.error) {
//     case 0:
//       break
//     case 2:
//       // alert('内存不足')
//       Notification.error({
//         title: '',
//         message: '内存不足'
//       })
//       break
//     case 4096:
//       // alert('文件不存在')
//       Notification.error({
//         title: '',
//         message: '文件不存在'
//       })
//       break
//     case 4106:
//       // alert('打开文件个数超过最大限制')
//       Notification.error({
//         title: '',
//         message: '打开文件个数超过最大限制'
//       })
//       break
//     case 12288:
//       // alert('下载文件失败')
//       Notification.error({
//         title: '',
//         message: '下载文件失败'
//       })
//       break
//     case 32768:
//       // alert('系统繁忙')
//       Notification.error({
//         title: '',
//         message: '系统繁忙'
//       })
//       break
//   }
// }
// /**
//  * 重写调用获取嫌疑标记后的onMessage方法
//  */
// viProbeWsCallback.get_mark_as_base64 = function (jsonObj) {
//   console.debug('OnMessage call #' + jsonObj.command + '(rewrite) method')
//   if (jsonObj.error === 0) {
//     getMarkBase64Success = true
//     markBase64 = jsonObj.result
//   } else {
//     getMarkBase64Success = false
//     markBase64 = null
//     errorCode = jsonObj.error
//   }
// }
// /**
//  * 重写调用获取新嫌疑标记后的onMessage方法
//  */
// viProbeWsCallback.get_new_marks = function (jsonObj) {
//   console.debug('OnMessage call #' + jsonObj.command + '(rewrite) method')
//   if (jsonObj.error === 0) {
//     getNewMarksSuccess = true
//     markNew = jsonObj.result
//   } else {
//     getNewMarksSuccess = false
//     markNew = null
//     errorCode = jsonObj.error
//   }
// }
// /**
//  * 重写调用上传ViProbe中审图信息后的onMessage方法
//  */
// viProbeWsCallback.upload = function (jsonObj) {
//   console.debug('OnMessage call #' + jsonObj.command + '(rewrite) method')
//   if (jsonObj.result && jsonObj.error === 0) {
//     uploadImageAnalysisSuccess = true
//   } else {
//     uploadImageAnalysisSuccess = false
//     errorCode = jsonObj.error
//   }
// }
// /**
//  * 重写调用获取嫌疑标记后的onMessage方法
//  */
// viProbeWsCallback.get_image_as_base64 = function (jsonObj) {
//   console.debug('OnMessage call #' + jsonObj.command + '(rewrite) method')
//   if (jsonObj.error === 0) {
//     getImageInViProbeBase64Success = true
//     imageInViProbeBase64 = jsonObj.result
//   } else {
//     getImageInViProbeBase64Success = false
//     imageInViProbeBase64 = null
//     errorCode = jsonObj.error
//   }
// }
export {
  getMarkBase64Success,
  getNewMarksSuccess,
  getMarkAsBase64,
  markNew,
  getNewMarks,
  openImageAndAnalysisInfoInViProbe,
  closeImageInViProbe,
  uploadImageAnalysisInfo,
  uploadImageAnalysisSuccess,
  reconnectToViProbeFailedNew
}
