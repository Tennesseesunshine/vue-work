import Vue from 'vue'
import vm from '@/main.js'
import { Notification } from 'element-ui'
import createBlobUrl from './apiDownloadFile'
/**
 * 按照index将数组拆分为两个
 * @param {*} arr 需要拆分的数组
 * @param {*} index 从哪里拆分
 * @return {left, right} 返回值 index左侧和右侧的数组
 */
const splitArr = (arr, index) => {
  if (Array.isArray(arr) && index !== 'NaN') {
    let left = arr.slice()
    let right = arr.slice()
    return {
      left: left.splice(0, index),
      right: right.splice(index, arr.length)
    }
  }
}

/**
 * 格式化时间工具
 * @param {*} inputTime 时间戳
 * @param {*} symbol 年月日国际化时间 默认给一个时间为中文
 * @return 格式化之后的当前时间
 */
const formatDateTime = (inputTime, symbol) => {
  if (inputTime) {
    var date = new Date(inputTime)
    var MM = date.getMonth() + 1
    var dd = date.getDate()
    var HH = date.getHours()
    var mm = date.getMinutes()
    var ss = date.getSeconds()
    var time = {
      yyyy: date.getFullYear(),
      MM: MM < 10 ? '0' + MM : MM,
      dd: dd < 10 ? '0' + dd : dd,
      HH: HH < 10 ? '0' + HH : HH,
      mm: mm < 10 ? '0' + mm : mm,
      ss: ss < 10 ? '0' + ss : ss
    }
    return symbol.replace(/([a-z])(\1)*/gi, m => {
      return time[m]
    })
  }
  return null
}

/**
 * 数组remove方法（index） 返回一个新数组
 * 使用: const data = arr.remove(1, [1, 2, 3]) // [1, 3]
 */
const remove = (dx, array) => {
  let arr = array.slice()
  if (isNaN(dx) || dx > arr.length) {
    return false
  }
  for (var i = 0, n = 0; i < arr.length; i++) {
    if (arr[i] !== arr[dx]) {
      arr[n++] = arr[i]
    }
  }
  arr.length -= 1
  return arr
}

/**
 * 判断图片是否存在
 * @param {*} url 图片地址
 * @return 图片路径是否存在服务器上  布尔值
 */
const isExitUrl = url => {
  let xmlHttp
  if (window.ActiveXObject) {
    xmlHttp = new ActiveXObject('Microsoft.XMLHTTP')
  } else if (window.XMLHttpRequest) {
    xmlHttp = new XMLHttpRequest()
  }
  try {
    xmlHttp.open('Get', url, false)
    xmlHttp.send()
    if (!url) {
      return false
    }
    if (xmlHttp.status === 200) {
      return true
    } else {
      return false
    }
  } catch (error) {
    return false
  }
}

// 将对象元素转换成字符串以作比较
const obj2key = (obj, keys) => {
  let n = keys.length
  let key = []
  while (n--) {
    key.push(obj[keys[n]])
  }
  return key.join('|')
}
// 数组去重操作  含对象结构
const uniqeByKeys = (array, keys) => {
  let arr = []
  let hash = {}
  for (let i = 0, j = array.length; i < j; i++) {
    let k = obj2key(array[i], keys)
    if (!(k in hash)) {
      hash[k] = true
      arr.push(array[i])
    }
  }
  return arr
}

/**
 * 根据传入的国际化字段排序下拉选项
 * @param arr =>[],  需要排序的数组, lang 按照当前语言需要排序的国际化zh/en/ru
 * @return arr [] 按照首字母排序
 */
const sortByFirstLetter = ({ arr, lang }) => {
  let sortArr = arr.slice()
  return sortArr.sort(function (str1, str2) {
    return str1.localeCompare(str2, lang)
  })
}
/**
 * 下载图片
 * @param {*} href a标签连接可以拼接参数
 * @param {*} filename 文件名称 默认为空
 */
const saveFile = (href, filename = '', count) => {
  if (count > 65535) {
    Notification.warning({
      title: vm.$t('ViBorder.common.warning'),
      message: vm.$t('ViBorder.businessLogicCommon.exportFail'),
      offset: window.config.notifyOffset
    })
    return false
  } else {
    let link = document.createElementNS('http://www.w3.org/1999/xhtml', 'a') // 创建一个带有命名空间的a标签
    link.href = href
    link.target = '_blank'
    link.download = filename
    link.click()
  }
}

/**
 * 过滤空字段
 * @param {*} options 要过滤的参数对象
 * @return {*} param 过滤之后的参数
 */
const isNull = (parm) => {
  return parm === null || parm === void 0 || parm === ''
}
const filterNullFields = (options) => {
  let param = {}
  if (Object.keys(options).length > 0) {
    for (let key in options) {
      if (!isNull(options[key]) || options[key] === 0) {
        Object.assign(param, { [key]: options[key] })
      }
    }
  }
  return param
}
const customValidateTimeRange = (rule, value, callback) => {
  if (value && value.length && value[0] && value[1]) {
    let d1 = value[0] && value[0].substring(0, 10)
    let d2 = value[1] && value[1].substring(0, 10)
    let tmp = d1 && d1.split('-')
    let date1 = tmp.length && new Date(tmp[0], tmp[1] - 1, tmp[2])
    tmp = d2 && d2.split('-')
    let date2 = tmp.length && new Date(tmp[0], tmp[1] - 1, tmp[2])
    if ((date2.getTime() - date1.getTime()) > 365 * 24 * 60 * 60 * 1000) {
      callback(new Error(vm.$t('ViBorder.IC.oneYearInterval')))
    } else {
      callback()
    }
  }
}

const formatSeconds = (t) => {
  if (t === null) {
    t = 0
  }
  let s = parseInt(t)
  let m = 0
  let h = 0
  if (s > 60) {
    m = parseInt(t / 60)
    s = parseInt(t % 60)
    if (m > 60) {
      h = parseInt(m / 60)
      m = parseInt(m % 60)
    }
  }
  h = h < 10 ? `0${h}` : h
  m = m < 10 ? `0${m}` : m
  s = s < 10 ? `0${s}` : s
  return `${h} : ${m} : ${s} `
}

const fdt = (inputTime, symbol = '-') => {
  if (inputTime) {
    let date = new Date(inputTime)
    let y = date.getFullYear()
    let m = date.getMonth() + 1
    m = m < 10 ? '0' + m : m
    let d = date.getDate()
    d = d < 10 ? '0' + d : d
    let h = date.getHours()
    h = h < 10 ? '0' + h : h
    let minute = date.getMinutes()
    let second = date.getSeconds()
    minute = minute < 10 ? '0' + minute : minute
    second = second < 10 ? '0' + second : second
    return `${d}${symbol}${m}${symbol}${y} ${h}:${minute}:${second}`
  }
  return null
}
const resetTime = () => {
  // 时间范围后期可以提出来当做参数
  let timeStr = []
  let pre = new Date()
  let arr = [`${new Date(pre.setMonth(pre.getMonth() - 3))}`, `${new Date()}`]
  arr.forEach((e, idx) => {
    let date = fdt(e)
    let ymdArr = date.split(' ')[0].split('-')
    let ymd = `${ymdArr[2]}-${ymdArr[1]}-${ymdArr[0]} `
    if (idx === 0) {
      ymd = ymd + `00:00:00`
    } else {
      ymd = ymd + `23:59:59.999`
    }
    timeStr.push(ymd)
  })
  return timeStr
}

export {
  splitArr,
  formatDateTime,
  formatSeconds,
  remove,
  isExitUrl,
  uniqeByKeys,
  sortByFirstLetter,
  saveFile,
  filterNullFields,
  customValidateTimeRange,
  resetTime,
  createBlobUrl
}
