window.config = {
  baseUrl: window.location.origin + '/',
  scannerUrl: 'http://KAZ-FILE:8080/',
  initLang: 'ru-RU',
  upms: 'upmsapi',
  fileServerUrl: 'ViStore/v1/kaz/share', // 文件服务器地址
  notifyOffset: 65, // this.$notify提示框的位置
  fileFolder: {
    lprFolder: '/lpr',
    ccrFolder: '/ccr',
    photoFolder: '/photo',
    videoFolder: '/video',
    manifestFolder: '/manifest',
    recheckFolder: '/recheck',
    niiFolder: '/nii'
  },
  Vitracking: {
    baseUrl: window.location.origin + '/',
    commonUrl: 'platform',
    urpmUrl: {
      web: 'upms',
      api: 'upmsapi'
    }
  },
  rtsp: {
    MRA: {
      rtspPath:
      'rtsp://admin:Nuctech123@192.168.111.74:554/h264/ch3/sub/av_stream',
      bitrate: '50k'
    },
    ICIA: {
      rtspPath:
        'rtsp://admin:Nuctech123@192.168.111.53:554/h264/ch3/sub/av_stream',
      bitrate: '50k'
    },
    OCIA: {
      rtspPath:
        'rtsp://admin:Nuctech123@192.168.111.73:554/h264/ch3/sub/av_stream',
      bitrate: '50k'
    }
  },
  devScanImg: false, // 是否可以扫描图片 生产为false
  baseErrorCode: 'E01', // upms的错误码
  commonUrl: 'v1',
  projectName: 'viborderapi',
  viborderStompWebsocket: '/stompWebsocket', // ws uri
  scannerWebSocket: 'wss://ws.adapter.nuctech:9981', // scan url
  rfidWebSocket: 'wss://ws.adapter.nuctech:9980', // rfid url
  timeout: 10000, // 接口请求超时时间设置
  lotOfDataTimeout: 100000, // 导出大数据的时候请求超时设置
  size: 5000, // 这里时导出数据最多5000条
  exportSizeMount: 10000,
  refreshTime: 30000 // 页面定时刷新间隔
}
